unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  LCLTranslator, Spin,
  BCP_47Form, BCP_47;



type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Btn_Translate: TButton;
    Button3: TButton;
    Button4: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    Edt_Language: TEdit;
    Label1: TLabel;
    SpE_FontSize: TSpinEdit;
    procedure Button1Click(Sender: TObject);      
    procedure Btn_TranslateClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure CheckBox4Change(Sender: TObject);
    procedure CheckBox5Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpE_FontSizeChange(Sender: TObject);

    private
      procedure InitFormBCP47;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

// init FormBCP47
procedure TForm1.InitFormBCP47;
begin
  FormBCP47:=TFormBCP47.Create(Form1);
  FormBCP47.Left:=Left + Width + 8;
  FormBCP47.Top:=Top;
  FormBCP47.Font.Size:=SpE_FontSize.Value;
end;


// open FormBCP47
procedure TForm1.Button1Click(Sender: TObject);
begin
  if FormBCP47 = nil then
  InitFormBCP47;
  Button3Click(nil);
end;

// translate - Form and FormBCP47
procedure TForm1.Btn_TranslateClick(Sender: TObject);
var
  alang: String;
begin
  with Btn_Translate do
  if Tag = 0 then
  begin
    alang:='de'; Tag:=1;
  end
  else
  if Tag = 1 then
  begin
    alang:='it'; Tag:=2;
  end
  else
  begin
    alang:='en'; Tag:=0;
  end;



  SetDefaultLang(alang);       // transalte the Form
  if FormBCP47 = nil then
  InitFormBCP47;
  FormBCP47.Translate(alang);  // translate the FormBCP47
end;

// Set Lang Favs
procedure TForm1.Button2Click(Sender: TObject);
var
  sl:TStringList;
begin
  sl:=TStringList.Create;
  sl.CommaText:='ger,en,fra';
  FormBCP47.Set_OftenUsedLangs(sl);
  sl.Free;
end;

// edit language
procedure TForm1.Button3Click(Sender: TObject);
begin
  if FormBCP47 = nil then Exit;
  FormBCP47.EditLanguage(Edt_Language.Text);

  if FormBCP47.LanguageTag <> '' then
  Edt_Language.Text:=FormBCP47.LanguageTag;
end;

// Set Region Favs
procedure TForm1.Button4Click(Sender: TObject);
var
  sl:TStringList;
begin
  sl:=TStringList.Create;
  sl.CommaText:='de,us,dd';
  FormBCP47.Set_OftenUsedRegions(sl);
  sl.Free;
end;

// ChB - change Deprecated Allowed
procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  TFormBCP47.DeprecatedAllowed:=CheckBox1.Checked;
end;

// ChB - CodesAtFirstPlace
procedure TForm1.CheckBox2Change(Sender: TObject);
begin
  TFormBCP47.CodesAtFirstPlace:=CheckBox2.Checked;
end;

// ChB - Only often used languages changed
procedure TForm1.CheckBox3Change(Sender: TObject);
begin
  TFormBCP47.LanguagesOftenOnly:=CheckBox3.Checked;
end;

// ChB - Use_ISO639_3_and_5
procedure TForm1.CheckBox4Change(Sender: TObject);
begin
  TFormBCP47.Use_ISO639_3_and_5:=CheckBox4.Checked;
end;

// ChB - Only often used Regions
procedure TForm1.CheckBox5Change(Sender: TObject);
begin
  TFormBCP47.RegionsOftenOnly:=CheckBox5.Checked;
end;

// onForm create
procedure TForm1.FormCreate(Sender: TObject);
begin
  Left:=200;
end;

// SpE - change FormBCP47 Font size
procedure TForm1.SpE_FontSizeChange(Sender: TObject);
begin
  if FormBCP47 <> nil then
  FormBCP47.Font.Size:=SpE_FontSize.Value;
  //FormBCP47.NewFontSize:=SpE_FontSize.Value;
end;




// TODO:



end.

