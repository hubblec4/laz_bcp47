const
  bcp47_re: String
{$IF FPC_FullVersion >= 30200}
//='^(?:(?:(?:(?:([a-zA-Z]{2,3})((?:-[a-zA-Z]{3}){0,3})?)|([a-zA-Z]{4})|([a-zA-Z]{5,8}))(?:-([a-zA-Z]{4}))?(?:-([a-zA-Z]{2}|[0-9]{3}))?((?:-(?:[a-zA-Z0-9]{5,8}|[0-9][a-zA-Z0-9]{3}))*)((?:-[a-wA-WyYzZ0-9](?:-[a-zA-Z0-9]{2,8})+)*)(?:-[xX]((?:-[a-zA-Z0-9]{1,8})+))?)|(?:[xX]((?:-[a-zA-Z0-9]{1,8})+))|(en-GB-oed|i-ami|i-bnn|i-default|i-enochian|i-hak|i-klingon|i-lux|i-mingo|i-navajo|i-pwn|i-tao|i-tay|i-tsu|sgn-BE-FR|sgn-BE-NL|sgn-CH-DE|art-lojban|cel-gaulish|no-bok|no-nyn|zh-guoyu|zh-hakka|zh-min|zh-min-nan|zh-xiang))$'
='^'
+'(?:'
+  '(?:'                                         // full language tag
+    '(?:'                                       // language + extlangs codes
+      '(?:'
+        '('                                     // 1: language2-3
+          '[a-zA-Z]{2,3}'
+        ')'
+        '(?:'                                   // 2: extlangs
+          '-([a-zA-Z]{3})'
+        ')?'
+      ')'
+      '|'                                       // 1. alternative
+      '('                                       // 3: language4
+        '[a-zA-Z]{4}'
+      ')'
+      '|'                                       // 2. alternative
+      '('                                       // 4: language5-8
+        '[a-zA-Z]{5,8}'
+      ')'
+    ')'

+    '(?:-'
+      '([a-zA-Z]{4})'                           // 5: script
+    ')?'

+    '(?:-'
+      '('                                       // 6: region
+        '[a-zA-Z]{2}'
+        '|'
+        '[0-9]{3}'
+      ')'
+    ')?'

+    '('                                         // 7: variants
+      '(?:-'
+        '(?:'
+          '[a-zA-Z0-9]{5,8}'
+          '|'
+          '[0-9][a-zA-Z0-9]{3}'
+        ')'
+      ')*'
+    ')'

+    '('                                         // 8: extensions
+      '(?:-'
+        '[a-wA-WyYzZ0-9]'
+        '(?:-'
+          '[a-zA-Z0-9]{2,8}'
+        ')+'
+      ')*'
+    ')'

+    '(?:-[xX]'
+      '('                                       // 9: privateuse
+        '(?:-'
+          '[a-zA-Z0-9]{1,8}'
+        ')+'
+      ')'
+    ')?'
+  ')'                                           // end full language tag
+  '|'
+  '(?:[xX]'
+    '('                                         // 10: privateuse global
+      '(?:-'
+        '[a-zA-Z0-9]{1,8}'
+      ')+'
+    ')'
+  ')'
+')'
+'$'
;

// RegExpr groups
rgLanguage         = 1;
rgExtLangs         = 2;
rgLanguage4        = 3;
rgLanguage5_8      = 4;
rgScript           = 5;
rgRegion           = 6;
rgVariants         = 7;
rgExtensions       = 8;
rgPrivateUse       = 9;
rgPrivateUseGlobal = 10;

rexExtensions      = '([a-wA-WyYzZ](?:-[a-zA-Z0-9]{2,8})+)';


{$ELSE}
//='^((((([a-zA-Z]{2,3})((-[a-zA-Z]{3}){0,3})?)|([a-zA-Z]{4})|([a-zA-Z]{5,8}))(-([a-zA-Z]{4}))?(-([a-zA-Z]{2}|[0-9]{3}))?((-([a-zA-Z0-9]{5,8}|[0-9][a-zA-Z0-9]{3}))*)((-[a-wA-WyYzZ0-9](-[a-zA-Z0-9]{2,8})+)*)(-[xX]((-[a-zA-Z0-9]{1,8})+))?)|([xX]((-[a-zA-Z0-9]{1,8})+))|(en-GB-oed|i-ami|i-bnn|i-default|i-enochian|i-hak|i-klingon|i-lux|i-mingo|i-navajo|i-pwn|i-tao|i-tay|i-tsu|sgn-BE-FR|sgn-BE-NL|sgn-CH-DE|art-lojban|cel-gaulish|no-bok|no-nyn|zh-guoyu|zh-hakka|zh-min|zh-min-nan|zh-xiang))$'
='^'
+'('                                             // 1: uninteresting
+  '('                                           // 2: full language tag
+    '('                                         // 3: language + extlangs codes
+      '('                                       // 4:
+        '('                                     // 5: language2-3
+          '[a-zA-Z]{2,3}'
+        ')'
+        '('                                     // 6:
+          '-([a-zA-Z]{3})'                      // 7: extlang
+        ')?'
+      ')'
+      '|'                                       // 1. alternative
+      '('                                       // 8: language4
+        '[a-zA-Z]{4}'
+      ')'
+      '|'                                       // 2. alternative
+      '('                                       // 9: language5-8
+        '[a-zA-Z]{5,8}'
+      ')'
+    ')'

+    '(-'                                        // 10:
+      '([a-zA-Z]{4})'                           // 11: script
+    ')?'

+    '(-'                                        // 12:
+      '('                                       // 13: region
+        '[a-zA-Z]{2}'
+        '|'
+        '[0-9]{3}'
+      ')'
+    ')?'

+    '('                                         // 14: variants
+      '(-'                                      // 15:
+        '('                                     // 16:
+          '[a-zA-Z0-9]{5,8}'
+          '|'
+          '[0-9][a-zA-Z0-9]{3}'
+        ')'
+      ')*'
+    ')'

+    '('                                         // 17: extensions
+      '(-'                                      // 18:
+        '[a-wA-WyYzZ0-9]'
+        '(-'                                    // 19:
+          '[a-zA-Z0-9]{2,8}'
+        ')+'
+      ')*'
+    ')'

+    '(-[xX]'                                    // 20:
+      '('                                       // 21: privateuse
+        '(-'                                    // 22:
+          '[a-zA-Z0-9]{1,8}'
+        ')+'
+      ')'
+    ')?'
+  ')'                                           // end full language tag
+  '|'
+  '([xX]'                                       // 23:
+    '('                                         // 24: privateuse global
+      '(-'                                      // 25:
+        '[a-zA-Z0-9]{1,8}'
+      ')+'
+    ')'
+  ')'
+')'
+'$'
;

// RegExpr groups
rgLanguage         = 5;
rgExtLangs         = 7;
rgLanguage4        = 8;
rgLanguage5_8      = 9;
rgScript           = 11;
rgRegion           = 13;
rgVariants         = 14;
rgExtensions       = 17;
rgPrivateUse       = 21;
rgPrivateUseGlobal = 24;

rexExtensions      = '([a-wA-WyYzZ](-[a-zA-Z0-9]{2,8})+)';
{$IFEND}

