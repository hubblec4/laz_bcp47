unit BCP_47;

{$mode objfpc}{$H+}

{$ModeSwitch advancedrecords}

{$define UseMatroskaWarning}

interface

uses
  Classes, SysUtils, RegExpr, fgl, BCP_47_RS;


type

  { TLanguage }

  TLanguage             = record
    Alpha_2_Code        : String;
    Alpha_3_Code        : String;
    Terminology_Code    : String;
    Is_Part_of_ISO639_2 : Boolean;
    Is_Part_of_ISO639_3 : Boolean;
    Is_Part_of_ISO639_5 : Boolean;

    { BCP-47 }
    Suppress_Script     : String;  // this Script should not be used with the
                                   // language subtag
    // deprecated language subtags for reading only and must be transformed
    // to the Preferred_Value for writing
    Deprecated          : Boolean; // the subtag is no longer used
    Preferred_Value     : String;  // this subtag should be used

    Name                : String;

    function ShortestCode: String;
    function Get_Caption(const codefirst: Boolean;
              const shortest: Boolean = false): String;
    function CodeMatch(const aCode: String): Boolean;
  end;
  PLanguage             = ^TLanguage;

  { TExtLang }

  TExtLang              = record
    Alpha_3_Code        : String;
    Deprecated          : Boolean; // the subtag is no longer used
    Prefix              : String;  // can only be used with this Language subtag
    Name                : String;

    function Get_Caption(const codefirst: Boolean): String;
  end;
  PExtLang              = ^TExtLang;

  { TScript }

  TScript               = record
    Alpha_4_Code        : String;
    Numeric             : String;
    Name                : String;

    function Get_Caption(const codefirst: Boolean): String;
  end;
  PScript               = ^TScript;

  { TRegion }

  TRegion               = record   // region + country + user codes
    Alpha_2_Code        : String;
    Alpha_3_Code        : String;                   
    Numeric             : String;  // 3 digit number
    Is_Part_of_ISO3166_1: Boolean;
    Deprecated          : Boolean; // the subtag is no longer used
    // a preferred value is not always present
    Preferred_Value     : String;  // this subtag should be used
    Name                : String;
    Name_official       : String;

    function Get_Code: String;
    function Get_Caption(const codefirst: Boolean): String;
    function CodeMatch(const aCode: String): Boolean;
  end;
  PRegion               = ^TRegion;

  { TVariant }

  TVariant              = record
    Code                : String;
    Deprecated          : Boolean;
    Name                : String;
    Prefixes            : String;

    function Get_Caption(const codefirst: Boolean): String;
  end;
  PVariant              = ^TVariant;

  { TExtensions }

  TExtensions           = record
    Identifier          : Char;
    Name                : String;

    function Get_Caption(const codefirst: Boolean): String;
  end;
  PExtensions           = ^TExtensions;

  { TGrandfathered }

  TGrandfathered        = record
    Tag                 : String;
    Deprecated          : Boolean;
    Preferred_Value     : String;  // this subtag should be used
    Name                : String;

    function Get_Caption(const codefirst: Boolean): String;
  end;
  PGrandfathered        = ^TGrandfathered;

  { TRedundant - only for serching prefered values }

  TRedundant            = record
    Tag                 : String;
    Preferred_Value     : String;
  end;


  { TLanguageBCP47 }

  TLanguageBCP47        = class
   private              // vars
    FErrorText          : String;
    FWarnings           : TStringList;
    FInfoText           : String;
    FIs_Valid           : Boolean;
    FLanguageTag        : String;

    { Subtags }
    FLanguage           : String;
    FLanguageData       : PLanguage;
    FExtLang            : String;
    FExtLangData        : PExtLang;
    FScript             : String;
    FScriptData         : PScript;
    FRegion             : String;
    FRegionData         : PRegion;
    FVariants           : TStringList;
    FExtensions         : TStringList;
    FPrivateUses        : TStringList;
    FGrandfathered      : String;
    FGrandfatheredData  : PGrandfathered;

    // Normalization
    FCanonicalForm      : String;
    FExtLangSubtagsForm : String;

   private              // methodes
    function Format_LanguageIntern: String;

    function Parse_Language(const code: String): Boolean;
    function Parse_ExtLangs(const code: String): Boolean;
    function Parse_Script(const code: String): Boolean;
    function Parse_Region(const code: String): Boolean;
    function Parse_Variants(const code: String): Boolean;
    function Parse_Extensions(const code: String): Boolean;
    procedure Parse_Privates(const code: String);
    function Parse_Grandfathered(const code: String): Boolean;

    procedure Normalization;

   protected
    class function ParseIntern(const LngTag: String;
           const isValid: Boolean=false): TLanguageBCP47; static;



   public               // methodes
    constructor Create;
    destructor Destroy; override;

    class function Parse(const LngTag: String): TLanguageBCP47; static;

    procedure SetLanguageData(const pLng: PLanguage);
    procedure SetVariants(const sl: TStringList); 
    procedure SetExtensions(const sl: TStringList);
    procedure SetPrivateUses(const sl: TStringList);

   public               // properties
    property Is_Valid: Boolean read FIs_Valid;
    property ErrorText: String read FErrorText;
    property InfoText: String read FInfoText;
    property Warnings: TStringList read FWarnings;
    property LanguageTag: String read Format_LanguageIntern;
    property CanonicalForm: String read FCanonicalForm;
    property ExtLangSubtagsForm: String read FExtLangSubtagsForm;

    property Language: String read FLanguage write FLanguage;
    property LanguageData: PLanguage read FLanguageData write SetLanguageData;
    property ExtLang: String read FExtLang write FExtLang;
    property Script: String read FScript write FScript;
    property ScriptData: PScript read FScriptData;
    property Region: String read FRegion write FRegion;
    property RegionData: PRegion read FRegionData;
    property Variants: TStringList read FVariants write SetVariants;
    property Extensions: TStringList read FExtensions write SetExtensions;
    property PrivateUses: TStringList read FPrivateUses write SetPrivateUses;
    property Grandfathered: String read FGrandfathered write FGrandfathered;

  end;


  function Find_Language(const LngCode: String): PLanguage;
  function Find_Region(const aRegion: String): PRegion;
  function Find_Grandfathered(const gfathered: String): PGrandfathered;


const
  LanguageArray         : {$i language.inc};

  ExtLangArray          : {$i extlang.inc};

  ScriptArray           : {$i script.inc};

  RegionArray           : {$i regions.inc};

  VariantArray          : {$i variants.inc};

  ExtensionsArray       : {$i extensions.inc};

  GrandfatheredArray    : {$i grandfathered.inc};

  RedundantArray        : {$i redundant_simple.inc};




implementation

{$i bcp47_re.inc}



// find the language
function Find_Language(const LngCode: String): PLanguage;
var
  a: Integer;
  lng: String;
begin
  Result:=nil;
  if LngCode = '' then Exit;
  lng:=LowerCase(LngCode);
  for a:=0 to High(LanguageArray) do with LanguageArray[a] do
  if CodeMatch(lng) then
  Exit(@LanguageArray[a]);

  // very old language codes
  case lng of
    'scr': Exit(Find_Language('hrv'));
    'scc': Exit(Find_Language('srp'));
    'mol': Exit(Find_Language('rum'));
  end;
end;


// find the extended language
function Find_ExtLang(const ExtlangCode: String): PExtLang;
var
  a: Integer;
  extlng: String;
begin
  Result:=nil;
  if ExtlangCode = '' then Exit;
  extlng:=LowerCase(ExtlangCode);
  for a:=0 to High(ExtLangArray) do
  if ExtLangArray[a].Alpha_3_Code = extlng then
  Exit(@ExtLangArray[a]);
end;


// find the Script
function Find_Script(const aScript: String): PScript;
var
  a: Integer;
  script: String;
begin
  Result:=nil;
  if aScript = '' then Exit;
  script:=LowerCase(aScript);
  script[1]:=Char(ord(script[1]) -32);
  for a:=0 to High(ScriptArray) do
  if ScriptArray[a].Alpha_4_Code = script then Exit(@ScriptArray[a]);
end;


// find the Region
function Find_Region(const aRegion: String): PRegion;
var
  a: Integer;
  region: String;
begin
  Result:=nil;
  if (aRegion = '') or (aRegion = '000') then Exit;

  if StrToIntDef(aRegion, -1) = -1 then // aRegion <> numbers
  region:=UpperCase(aRegion)
  else region:=aRegion;

  for a:=0 to High(RegionArray) do
  with RegionArray[a] do
  if CodeMatch(region) then
  Exit(@RegionArray[a]);
end;


// find the Variant
function Find_Variant(const aVariant: String): PVariant;
var
  a: Integer;
  variant: String;
begin
  Result:=nil;
  if aVariant = '' then Exit;
  variant:=LowerCase(aVariant);
  for a:=0 to High(VariantArray) do
  if VariantArray[a].Code = variant then Exit(@VariantArray[a]);
end;

// find the grandfathered
function Find_Grandfathered(const gfathered: String): PGrandfathered;
var
  a: Integer;
  gf: String;
begin
  Result:=nil;
  if gfathered = '' then Exit;
  gf:=LowerCase(gfathered);
  for a:=0 to High(GrandfatheredArray) do
  if LowerCase(GrandfatheredArray[a].Tag) = gf then
  Exit(@GrandfatheredArray[a]);
end;



{------- TLanguage ------------------------------------------------------------}

// get the shortest code
function TLanguage.ShortestCode: String;
begin
  if Alpha_2_Code <> '' then
  Result:=Alpha_2_Code
  else Result:=Alpha_3_Code;
end;

// get a caption
function TLanguage.Get_Caption(const codefirst: Boolean;
         const shortest: Boolean = false): String;
begin
  if shortest then
  Result:=ShortestCode
  else
  begin
    Result:=Alpha_2_Code;
    if Alpha_3_Code <> '' then
      if Result <> '' then
      Result += ',' + Alpha_3_Code
      else Result:=Alpha_3_Code;
  end;

  if codefirst then
  Result += ' ' + Name
  else Result:=Name + ' (' + Result + ')';
end;

// code match
function TLanguage.CodeMatch(const aCode: String): Boolean;
begin
  Result:= (Alpha_3_Code = aCode) or (Alpha_2_Code = aCode)
  or (Terminology_Code = aCode);
end;



{------- TExtLang -------------------------------------------------------------}

// get a caption
function TExtLang.Get_Caption(const codefirst: Boolean): String;
begin
  if codefirst then
  Result:=Alpha_3_Code + ' ' + Name
  else Result:=Name + ' (' + Alpha_3_Code + ')';
end;


{------- TScript --------------------------------------------------------------}

// get a caption
function TScript.Get_Caption(const codefirst: Boolean): String;
begin
  if codefirst then
  Result:=Alpha_4_Code + ' ' + Name
  else Result:=Name + ' (' + Alpha_4_Code + ')';
end;



{------- TRegion --------------------------------------------------------------}

// get code -> alpha2 code or numeric
function TRegion.Get_Code: String;
begin
  if Alpha_2_Code <> '' then
  Result:=Alpha_2_Code
  else Result:=Numeric;
end;

// get a caption
function TRegion.Get_Caption(const codefirst: Boolean): String;
begin
  if codefirst then
  Result:=Get_Code + ' ' + Name
  else Result:=Name + ' (' + Get_Code + ')';
end;

// code match
function TRegion.CodeMatch(const aCode: String): Boolean;
begin
  Result:= (Alpha_2_Code = aCode) or (Numeric = aCode)
  or (Alpha_3_Code = aCode);
end;



{------- TVariant -------------------------------------------------------------}

// get a caption
function TVariant.Get_Caption(const codefirst: Boolean): String;
begin
  if codefirst then
  Result:=Code + ' ' + Name
  else Result:=Name + ' (' + Code + ')';
end;



{------- TExtensions ----------------------------------------------------------}

// get a caption
function TExtensions.Get_Caption(const codefirst: Boolean): String;
begin
  if codefirst then
  Result:=Identifier + ' ' + Name
  else Result:=Name + ' (' + Identifier + ')';
end;



{------- TGrandfathered -------------------------------------------------------}

// get a caption
function TGrandfathered.Get_Caption(const codefirst: Boolean): String;
begin
  if codefirst then
  Result:=Tag + ' ' + Name
  else Result:=Name + ' (' + Tag + ')';
end;





{---------- TLanguageBCP47 ----------------------------------------------------}


// constructor
constructor TLanguageBCP47.Create;
begin
  FVariants             :=TStringList.Create;
  FVariants.Delimiter   :='-';
  FExtensions           :=TStringList.Create;
  FExtensions.Delimiter :='-';
  FPrivateUses          :=TStringList.Create;
  FPrivateUses.Delimiter:='-';
  FWarnings             :=TStringList.Create;
end;

// destructor
destructor TLanguageBCP47.Destroy;
begin
  FVariants.Free;
  FExtensions.Free;
  FPrivateUses.Free;
  FWarnings.Free;
  inherited Destroy;
end;



// internal format
function TLanguageBCP47.Format_LanguageIntern: String;
begin
  Result:='';

  if FGrandfathered <> '' then
  begin
    Result:=FGrandfathered;
    Exit;
  end;

  Result:=FLanguage;

  if FExtLang <> '' then
  Result += Format('-%s', [LowerCase(FExtLang)]);

  if FScript <> '' then
  begin
    FScript:=LowerCase(FScript);
    FScript[1]:=Char(Ord(FScript[1]) -32);
    Result += Format('-%s', [FScript]);
  end;

  if FRegion <> '' then
  Result += Format('-%s', [UpperCase(FRegion)]);

  if FVariants.Count > 0 then
  Result += Format('-%s', [LowerCase(FVariants.DelimitedText)]);

  if FExtensions.Count > 0 then
  Result += Format('-%s', [LowerCase(FExtensions.DelimitedText)]);

  if FPrivateUses.Count > 0 then
  begin
    if Result <> '' then
    Result += '-';
    Result += Format('x-%s', [FPrivateUses.DelimitedText]);
  end;

  if pos('"', Result) > 0 then
  Result:=StringReplace(Result, '"', '', [rfReplaceAll]);
end;





// parse Language
function TLanguageBCP47.Parse_Language(const code: String): Boolean;
begin
  Result:=false;
  FLanguageData:=Find_Language(code);
  if FLanguageData = nil then
  begin
    FErrorText:=Format(rsNoValidLanguageCode, [code]);
    Exit;
  end;
  FLanguage:=FLanguageData^.ShortestCode;
  Result:=true;

  {$ifdef UseMatroskaWarning}
  if not FLanguageData^.Is_Part_of_ISO639_2 then
  FWarnings.Add(Format(rsWarningLanguageTag, [FLanguage]));
  {$endif}

  (* warning Deprecated *)
  if FLanguageData^.Deprecated then
  FWarnings.Add(Format(rsWarnDeprecatedLang, [FLanguage]));

end;

// parse Extended language
function TLanguageBCP47.Parse_ExtLangs(const code: String): Boolean;
begin
  Result:=true;
  FExtLangData:=Find_ExtLang(code);
  if FExtLangData = nil then
  begin
    FErrorText:=Format(rsNoValidExtLangCode, [code]);
    Exit(false);
  end;                                  
  FExtLang:=FExtLangData^.Alpha_3_Code;

  (* check prefix *)
  if FExtLangData^.Prefix <> FLanguage then
  begin
    FErrorText:=Format(rsNoValidExtLangPrefix,
    [FExtLang, FExtLangData^.Prefix]);
    Exit(false);
  end;
end;

// parse Script
function TLanguageBCP47.Parse_Script(const code: String): Boolean;
begin
  Result:=false;
  FScriptData:=Find_Script(code);
  if FScriptData = nil then
  begin
    FErrorText:=Format(rsNoValidScriptCode, [code]);
    Exit;
  end;
  FScript:=FScriptData^.Alpha_4_Code;

  (* warn suppress script *)
  if FScript = FLanguageData^.Suppress_Script then
  FWarnings.Add(Format(rsWarnSuppressScript, [FScript, FLanguage]));
  Result:=true;
end;

// parse Region
function TLanguageBCP47.Parse_Region(const code: String): Boolean;
var
  is_number: Boolean;
begin
  Result:=false;
  is_number:=StrToIntDef(code, -1) >= 0 ;
  FRegionData:=Find_Region(code);

  if FRegionData = nil then
  begin
    if is_number then
    FErrorText:=Format(rsNoValidRegionNumber, [code])
    else
    FErrorText:=Format(rsNoValidRegionCode, [code]);
    Exit;
  end;

  if is_number then
  FRegion:=FRegionData^.Numeric
  else FRegion:=FRegionData^.Alpha_2_Code;

  Result:=true;

  (* warning Deprecated *)
  if FRegionData^.Deprecated then
  FWarnings.Add(Format(rsWarnDeprecatedRegion, [FRegion]));
end;

// parse Variants
function TLanguageBCP47.Parse_Variants(const code: String): Boolean;
var
  aVariant: PVariant;
  entries: TStringArray;
  e: Integer;
  PrefixList: TStringList;
  current_prefix: String;

label
  FINISH;

begin
  Result:=false;
  entries:=code.Split('-');
  current_prefix:=Format_LanguageIntern;
  PrefixList:=TStringList.Create;

  for e:=1 to High(entries) do
  begin
    aVariant:=Find_Variant(entries[e]);
    if aVariant = nil then
    begin
      FErrorText:=Format(rsNoValidVariantCode, [entries[e]]);
      goto FINISH;
    end;
    (* check duplicate variant *)
    if FVariants.IndexOf(aVariant^.Code) > -1 then
    begin
      FErrorText:=Format(rsErrorVariantDouble, [aVariant^.Code]);
      goto FINISH;
    end;

    (* warning Deprecated *)
    if aVariant^.Deprecated then
    FWarnings.Add(Format(rsWarnDeprecatedVariant, [aVariant^.Code]));

    (* warning prefix: "suitable prefixes" *)
    if aVariant^.Prefixes <> '' then
    begin
      PrefixList.CommaText:=aVariant^.Prefixes;
      if PrefixList.IndexOf(current_prefix) = -1 then
      FWarnings.Add(Format(rsWarnVariantPrefix, [aVariant^.Code
      ,StringReplace(aVariant^.Prefixes, ',', ', ', [rfReplaceAll])]));
    end;
    current_prefix += '-' + aVariant^.Code;

    FVariants.AddObject(aVariant^.Code, TObject(aVariant));
  end;
  Result:=true;

FINISH:
  PrefixList.Free;
end;

// parse Extensions
function TLanguageBCP47.Parse_Extensions(const code: String): Boolean;
var
  reg: TRegExpr;
  e,e_count: Integer;
begin
  // extension sample : -a-2to8char-12345678-b-nextExt-b-thirdExt
  // first extension  : a-2to8char-12345678
  // second extension : b-nextExt
  // Extension init-char must be unique -> a-1111-a-2222 invalid

  Result:=false;
  reg:=TRegExpr.Create(rexExtensions); // ([a-wA-WyYzZ](?:-[a-zA-Z0-9]{2,8})+)
  if reg.Exec(code) then
  repeat
    FExtensions.Add(reg.Match[1]);
  until not reg.ExecNext;
  reg.Free;

  e_count:=FExtensions.Count;
  for e:=0 to e_count -1 do
  begin
    if not (LowerCase(FExtensions[e][1]) in ['u', 't']) then
    begin
      FErrorText:=Format(rsNoValidExtensionsId, [FExtensions[e][1]]);
      Exit;
    end;

    if e+1 < e_count then
    if FExtensions[e][1] = FExtensions[e+1][1] then
    begin
      FErrorText:=Format(rsExtensionsIdDuplicated,
      [FExtensions[e], FExtensions[e+1], FExtensions[e][1]]);
      Exit;
    end;
  end;

  Result:=true;
end;

// parse private use
procedure TLanguageBCP47.Parse_Privates(const code: String);
begin
  FPrivateUses.DelimitedText:=code.Substring(1);
end;

// parse Grandfathered
function TLanguageBCP47.Parse_Grandfathered(const code: String): Boolean;
begin
  Result:=true;
  FGrandfatheredData:=Find_Grandfathered(code);
  if FGrandfatheredData = nil then Exit(false);
  FGrandfathered:=FGrandfatheredData^.Tag;

  (* warning Deprecated *)
  if FGrandfatheredData^.Deprecated then
  FWarnings.Add(Format(rsWarnDeprecatedGrandfathered, [FGrandfathered]));
end;





// Normalization - canonical and extended language subtags form
procedure TLanguageBCP47.Normalization;
var
  extens_temp: TStringList = nil;
  extl_data: PExtLang;
  privUseGlobal: Boolean=false;
  r: Byte;

label
  FINISH_Canonical;

begin
  FLanguageTag:=Format_LanguageIntern;

  (* grandfathered *)
  if FGrandfatheredData <> nil then
  begin
    if FGrandfatheredData^.Preferred_Value <> '' then
    FCanonicalForm:=FGrandfatheredData^.Preferred_Value
    else
    FCanonicalForm:=FLanguageTag;
    goto FINISH_Canonical;
  end;

  (* variants *)
  // only one variant has a prefered value, "heploc" with prefix ja-Latn-hepburn
  // Perfered tag: ja-Latn-alalc97 - Preferred-Value: alalc97
  if FLanguageTag = 'ja-Latn-hepburn-heploc' then
  begin
    FCanonicalForm:='ja-Latn-alalc97';
    goto FINISH_Canonical;
  end;


  (* redundant *)
  for r:=0 to High(RedundantArray) do
  if RedundantArray[r].Tag = FLanguageTag then
  begin
    FCanonicalForm:=RedundantArray[r].Preferred_Value;
    goto FINISH_Canonical;
  end;


  (* language tag *)
  if FLanguageData <> nil then
  begin
    if FExtLang <> '' then
    FCanonicalForm:=FExtLang
    else
    if FLanguageData^.Preferred_Value <> '' then
    FCanonicalForm:=FLanguageData^.Preferred_Value
    else FCanonicalForm:=FLanguage;

    if FScript <> '' then
    FCanonicalForm += Format('-%s', [FScript]);

    if FRegionData <> nil then
    begin
      if FRegionData^.Preferred_Value <> '' then
      FCanonicalForm += Format('-%s', [FRegionData^.Preferred_Value])
      else FCanonicalForm += Format('-%s', [FRegion]);
    end;

    if FVariants.Count > 0 then
    FCanonicalForm += Format('-%s', [FVariants.DelimitedText]);

    if FExtensions.Count > 0 then
    begin
      extens_temp:=TStringList.Create;
      extens_temp.Text:=FExtensions.Text;
      extens_temp.Sort;
      FCanonicalForm += Format('-%s', [LowerCase(extens_temp.DelimitedText)]);
      extens_temp.Free;
    end;
  end;

  if FPrivateUses.Count > 0 then
  begin
    if FCanonicalForm <> '' then
    FCanonicalForm += '-'
    else privUseGlobal:=true;
    FCanonicalForm += Format('x-%s', [FPrivateUses.DelimitedText]);
  end;

  if pos('"', FCanonicalForm) > 0 then
  FCanonicalForm:=StringReplace(FCanonicalForm, '"', '', [rfReplaceAll]);
                    
  FINISH_Canonical:

  if LowerCase(FCanonicalForm) <> LowerCase(FLanguageTag) then
  FWarnings.Add(Format(rsWarnCanonical, [FCanonicalForm]));


  (* Extended language subtags form *)
  if privUseGlobal then
  begin
    FExtLangSubtagsForm:=FCanonicalForm;
  end
  else
  begin
    if pos('-', FCanonicalForm) > 0 then
    extl_data:=Find_ExtLang(copy(FCanonicalForm, 1, pos('-', FCanonicalForm)-1))
    else
    extl_data:=Find_ExtLang(FCanonicalForm);
    if extl_data <> nil then
    FExtLangSubtagsForm:=Format('%s-%s', [extl_data^.Prefix, FCanonicalForm])
    else
    FExtLangSubtagsForm:=FCanonicalForm;
  end;

  if LowerCase(FExtLangSubtagsForm) <> LowerCase(FLanguageTag) then
  FInfoText:=Format(rsInfoExtLangForm, [FExtLangSubtagsForm]);

end;




// Parse internal
class function TLanguageBCP47.ParseIntern(const LngTag: String;
  const isValid: Boolean): TLanguageBCP47;
var
  reg: TRegExpr;
begin
  Result:=TLanguageBCP47.Create;
  if LngTag = '' then
  begin
    Result.FErrorText:=rsNoValidBCPStructure;
    Exit;
  end;
  
  (* Grandfathered *)
  if Result.Parse_Grandfathered(LngTag) then
  begin
    Result.FIs_Valid:=true;
    Result.Normalization;
    Exit;
  end;

  while true do begin

  reg:=TRegExpr.Create(bcp47_re);
  if not reg.Exec(LngTag) then
  begin
    Result.FErrorText:=rsNoValidBCPStructure;
    Break;
  end;

  if reg.Match[rgPrivateUseGlobal] <> '' then    // global private use
  begin
    Result.Parse_Privates(reg.Match[rgPrivateUseGlobal]);
    Result.FIs_Valid:=true;
    Break;
  end;

  if reg.Match[rgLanguage] <> '' then
  if not Result.Parse_Language(reg.Match[rgLanguage]) then// language
  Break;

  if reg.Match[rgExtLangs] <> '' then            // Extended language
  if not Result.Parse_ExtLangs(reg.Match[rgExtLangs]) then Break;

  if reg.Match[rgLanguage4] <> '' then           // 4-letter language code
  begin
    Result.FErrorText:=rsNoSupportLanguage4Letter;
    Break;
  end;

  if reg.Match[rgLanguage5_8] <> '' then         // 5_to_8-letter language code
  begin
    Result.FErrorText:=rsNoSupportLanguage5_8Letter;
    Break;
  end;

  if (reg.Match[rgScript] <> '')                 // Script
  and (not Result.Parse_Script(reg.Match[rgScript])) then
  Break;

  if (reg.Match[rgRegion] <> '')                 // Region
  and (not Result.Parse_Region(reg.Match[rgRegion])) then
  Break;

  if reg.Match[rgVariants] <> '' then            // Variants
  if not Result.Parse_Variants(reg.Match[rgVariants]) then Break;

  if (reg.Match[rgExtensions] <> '')             // Extensions
  and (not Result.Parse_Extensions(reg.Match[rgExtensions])) then
  Break;

  if (reg.Match[rgPrivateUse] <> '') then        // Privates
  Result.Parse_Privates(reg.Match[rgPrivateUse]);

  Result.FIs_Valid:=true;
  Result.Normalization;

  Break;
  end;
  reg.Free;
end;







// parse a language bcp47 tag
class function TLanguageBCP47.Parse(const LngTag: String): TLanguageBCP47;
begin
  Result:=ParseIntern(LngTag);
end;

// set lnguage via pLanguage
procedure TLanguageBCP47.SetLanguageData(const pLng: PLanguage);
begin
  FLanguageData:=pLng;
  if pLng = nil then
  FLanguage:=''
  else FLanguage:=pLng^.ShortestCode;
end;

// Set Variants
procedure TLanguageBCP47.SetVariants(const sl: TStringList);
var
  s: Integer;
begin
  FVariants.Clear;
  if sl = nil then Exit;
  for s:=0 to sl.Count -1 do
  if sl[s] = '' then Continue
  else FVariants.Add(sl[s]);
end;

// Set Extension
procedure TLanguageBCP47.SetExtensions(const sl: TStringList);
var
  s: Integer;
begin
  FExtensions.Clear;
  if sl = nil then Exit;
  for s:=0 to sl.Count -1 do
  if sl[s] = '' then Continue
  else FExtensions.Add(sl[s]);
end;

// Set Private uses
procedure TLanguageBCP47.SetPrivateUses(const sl: TStringList);
var
  s: Integer;
begin
  FPrivateUses.Clear;
  if sl = nil then Exit;
  for s:=0 to sl.Count -1 do
  if sl[s] = '' then Continue
  else FPrivateUses.Add(sl[s]);
end;

















end.

