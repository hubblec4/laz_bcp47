unit BCP_47Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, ActnList, Menus, DividerBevel, Math, BCP_47, Translations;


type

  TStatusType = (stOK, stWarnig, stError, stInfo);

  TNormalizationForm = (nfNone, nfCanonical, nfExtLang);


  { TFormBCP47 }

  TFormBCP47 = class(TForm)
    AcL: TActionList;
    Act_Cancel: TAction;
    Act_OK: TAction;
    CoB_ExtLang: TComboBox;
    CoB_Grandfathered: TComboBox;
    CoB_Region: TComboBox;
    CoB_Script: TComboBox;
    CoB_Variants: TComboBox;
    CoB_FreeInput: TComboBox;
    CoB_Language: TComboBox;
    CoB_Extensions: TComboBox;
    DiB_Individual: TDividerBevel;
    DB_Grandfathered: TDividerBevel;
    Edt_Extensions: TEdit;
    Edt_PrivateUse: TEdit;
    ImL_16: TImageList;
    Img_Status: TImage;
    Lbl_ExtLang: TLabel;
    Lbl_Edit: TLabel;
    Lbl_Grandfathered: TLabel;
    Lbl_PrivateUse: TLabel;
    Lbl_Region: TLabel;
    Lbl_Script: TLabel;
    Lbl_Status: TLabel;
    Lbl_StatusInfo: TLabel;
    Lbl_Variants: TLabel;
    Lbl_FreeInput: TLabel;
    Lbl_Language: TLabel;
    Lbl_StatusSection: TLabel;
    Lbl_Extensions: TLabel;
    Mit_NoNormalization: TMenuItem;
    Mit_ExtLangAuto: TMenuItem;
    Mit_ExtLangOnce: TMenuItem;
    Mit_Break1: TMenuItem;
    Mit_CanonicalAuto: TMenuItem;
    Mit_CanonicalOnce: TMenuItem;
    Pan_PrivateUse: TPanel;
    Pan_Variants: TPanel;
    Pan_Edit: TPanel;
    Pan_Main: TPanel;
    Pan_Status: TPanel;
    Pan_Extensions: TPanel;
    PoM_Normalization: TPopupMenu;
    ScB_Comps: TScrollBox;
    SpB_Cancel: TSpeedButton;
    SpB_PrivateUseAdd: TSpeedButton;
    SpB_VariantsAdd: TSpeedButton;
    SpB_ExtensionsAdd: TSpeedButton;
    SpB_OK: TSpeedButton;
    SpB_Normalization: TSpeedButton;




    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);   
    procedure FormDestroy(Sender: TObject);

    procedure Act_OKExecute(Sender: TObject);
    procedure Act_CancelExecute(Sender: TObject);

    procedure CoB_FreeInputChange(Sender: TObject);

    procedure CoB_LanguageChange(Sender: TObject);
    procedure Set_CoB_Language;

    procedure CoB_ExtLangChange(Sender: TObject);
    procedure Set_CoB_ExtLangs;
                                                
    procedure CoB_ScriptChange(Sender: TObject);
    procedure Set_CoB_Script;

    procedure CoB_RegionChange(Sender: TObject);
    procedure Set_CoB_Region;

    procedure SpB_VariantsAddClick(Sender: TObject);
    procedure SpB_VariantDelClick(Sender: TObject);
    procedure CoB_VariantsChange(Sender: TObject);
    procedure Set_CoB_Variants;

    procedure SpB_ExtensionsAddClick(Sender: TObject);
    procedure SpB_ExtensionsDelClick(Sender: TObject);
    procedure ExtensionsChange(Sender: TObject);
    procedure Set_Comps_Extensions;

    procedure SpB_PrivateUseAddClick(Sender: TObject);
    procedure SpB_PrivteUseDelClick(Sender: TObject);
    procedure Edt_PrivateUseChange(Sender: TObject);
    procedure Set_Edt_PrivateUse;

    procedure CoB_GrandfatheredChange(Sender: TObject);
    procedure Set_CoB_Grandfathered;

    procedure SpB_NormalizationClick(Sender: TObject);
    procedure PoM_NormalizationClose(Sender: TObject);
    procedure Mit_Normalization_AutoClick(Sender: TObject);
    procedure Mit_Normalization_OnceClick(Sender: TObject);


  private               // vars
    FLangBCP47          : TLanguageBCP47;
    FUpdate             : Boolean;
    FNewUILang          : Boolean;

    FVariants           : TStringList;
    FExtensions         : TStringList;
    FPrivateUses        : TStringList;

    FLangTagIntern      : String; // current valid language BCP47 tag
    FCanonicalForm      : String;
    FExtLangForm        : String;
    FLanguageTag        : String; // language BCP47 tag for output

    FStatusComps        : TList;

    class var
    FNewFontSize        : Integer;
    FNewFontName        : String;
    FDeprecatedAllowed  : Boolean;
    FCodesAtFirstPlace  : Boolean;
    FLanguagesOftenUsed : TStringList;
    FLanguagesOftenOnly : Boolean;
    FUse_ISO639_3_and_5 : Boolean;
    FRegionsOftenUsed   : TStringList;
    FRegionsOftenOnly   : Boolean;
    FNormalizationAuto  : TNormalizationForm;
    FNormalizationOnce  : TNormalizationForm;

  private               // methods
    procedure InitLanguage_Cob;
    procedure InitExtLang_Cob;
    procedure InitScript_Cob;
    procedure InitRegion_Cob;
    procedure InitVariant_Cob;
    procedure InitExtensions_Cob;
    procedure InitGrandfathered_Cob;
    procedure InitNormalizationAuto;

    procedure SetComps;          
    procedure Translate_CoBItems;

    function Validate(const isFreeFormInput: Boolean=false): Boolean;

    procedure SetStatusInfo(const aBCP: TLanguageBCP47);
    procedure SetNormalizationItems;

    procedure AddFavoriteLangTag(const ALangTag: String);

    procedure DeprecatedAllowed_Changed;
    procedure CodesAtFirstPlace_Changed;
    procedure Set_CompsPosition;
    procedure Change_FontValues;
    procedure Change_CompsWidth;

  private               // class methods
    class procedure SetDeprecatedAllowed(const allow: Boolean); static;
    class procedure SetCodesAtFirstPlace(const first: Boolean); static;
    class procedure SetLanguagesOftenOnly(const only: Boolean); static;
    class procedure SetRegionsOftenOnly(const only: Boolean); static;
    class procedure SetUse_ISO639_3_and_5(const use_3_5: Boolean); static;
    class procedure SetNormalizationAuto(const nf: TNormalizationForm); static;


  public
    class procedure Set_OftenUsedLangs(const sl :TStringList); static;
    class procedure Set_OftenUsedRegions(const sl :TStringList); static;

    procedure Translate(const ALang: String);

    procedure EditLanguage(const aLang: String);

  public                // class properties
    class property DeprecatedAllowed:  Boolean read  FDeprecatedAllowed
                                               write SetDeprecatedAllowed;
    class property CodesAtFirstPlace:  Boolean read  FCodesAtFirstPlace
                                               write SetCodesAtFirstPlace;
    class property LanguagesOftenOnly: Boolean read  FLanguagesOftenOnly
                                               write SetLanguagesOftenOnly;
    class property RegionsOftenOnly:   Boolean read  FRegionsOftenOnly
                                               write SetRegionsOftenOnly;
    class property Use_ISO639_3_and_5: Boolean read  FUse_ISO639_3_and_5
                                               write SetUse_ISO639_3_and_5;

    class property NormalizationAuto: TNormalizationForm read FNormalizationAuto
                                      write SetNormalizationAuto default nfNone;

  public                // properties
    property LanguageTag: String read FLanguageTag;

  end;




  { TNewComps }

  TNewComps             = class
  public
    aButton             : TControl;
    aInput              : TControl;
    destructor Destroy; override;
  end;

  { TNewComps2 }

  TNewComps2            = class(TNewComps)
  public
    aInput2             : TControl;
    destructor Destroy; override;
  end;


  { TStatusComps }

  TStatusComps          = class
  public
    Lbl_StatusType      : TLabel;
    Lbl_StatusText      : TLabel;
    Img_StatusType      : TImage;
    destructor Destroy; override;
  end;



var
  FormBCP47: TFormBCP47 = nil;

implementation

uses BCP_47_RS;

const
  // Image index
  iiAdd    = 0;
  iiDel    = 1;
  iiOK     = 2;
  iiWarn   = 3;
  iiError  = 4;
  iiInfo   = 5;

  // max amount of Subtags
  maxVariants   = 100;
  maxExtensions = 2;
  maxPrivateUse = 250;

{$R *.lfm}


// set Anchors for a Control
procedure Set_Anchors(const aControl, refControl: TControl);
begin
  with aControl do
  begin
    AnchorSideTop.Control := refControl;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Top:=8;
    AnchorSideRight.Control := refControl;
    AnchorSideRight.Side := asrRight;
    AnchorSideLeft.Control := refControl;
    AnchorSideLeft.Side := asrLeft;
    Anchors := [akLeft, akRight, akTop];
  end;
end;

// set Anchors for a Button
procedure Set_Anchors_Button(const aControl, refControl: TControl);
begin
  with aControl do
  begin
    AnchorSideTop.Control := refControl;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := refControl;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Left:=6;
    Anchors := [akLeft, akTop];
  end;
end;

// set Combobox item width
procedure Set_CobItemWidth(const aCob: TComboBox);
var
  i: Integer; m: Integer = 0;
begin
  with aCob do
  begin
    for i:=0 to Items.Count -1 do
    m:=Max(FormBCP47.Canvas.GetTextWidth(Items[i]) +8, m);
    if Items.Count > DropDownCount then
    Inc(m, 24);
    ItemWidth:=m;
  end;
end;









(*------- TFormBCP47 ---------------------------------------------------------*)


// on Form create
procedure TFormBCP47.FormCreate(Sender: TObject);
begin
  if FNewFontSize = 0 then
  FNewFontSize:=8;
  if FNewFontName = '' then
  FNewFontName:='Tahoma';
  Top:=100;
  Left:=200;

  Visible:=false;
  FLangBCP47:=TLanguageBCP47.Create;

  FVariants:=TStringList.Create;
  FVariants.OwnsObjects:=true;
  FVariants.Add('');

  FExtensions:=TStringList.Create;
  FExtensions.OwnsObjects:=true;
  FExtensions.Add('');

  FPrivateUses:=TStringList.Create;
  FPrivateUses.OwnsObjects:=true;
  FPrivateUses.Add('');

  InitLanguage_Cob;
  InitExtLang_Cob;
  InitScript_Cob;
  InitRegion_Cob;
  InitVariant_Cob;
  InitExtensions_Cob;
  InitGrandfathered_Cob;
  InitNormalizationAuto;

  FStatusComps:=TList.Create;

  ImL_16.GetBitmap(iiOK, SpB_OK.Glyph);
  ImL_16.GetBitmap(iiError, SpB_Cancel.Glyph);
  ImL_16.GetBitmap(iiAdd, SpB_VariantsAdd.Glyph);
  ImL_16.GetBitmap(iiAdd, SpB_ExtensionsAdd.Glyph);
  ImL_16.GetBitmap(iiAdd, SpB_PrivateUseAdd.Glyph);
end;


// on Form Show
procedure TFormBCP47.FormShow(Sender: TObject);
begin
  if (FNewFontSize <> Font.Size)
  or (FNewFontName <> Font.Name) then
  Change_FontValues
  else
  if FNewUILang then
  begin
    Set_CompsPosition;
    FNewUILang:=false;
  end;
  Validate(true);
  if FLangBCP47.Is_Valid then
  AddFavoriteLangTag(FLangTagIntern);
  FLanguageTag:='';
  CoB_FreeInput.SetFocus;
end;


// onForm destroy
procedure TFormBCP47.FormDestroy(Sender: TObject);
begin
  FLangBCP47.Free;
  FVariants.Free;
  FExtensions.Free;
  FPrivateUses.Free;
  if FLanguagesOftenUsed <> nil then FLanguagesOftenUsed.Free;
  if FRegionsOftenUsed <> nil then FRegionsOftenUsed.Free;
end;


// Action - OK - close Form with a valid language tag
procedure TFormBCP47.Act_OKExecute(Sender: TObject);
begin
  if not SpB_OK.Enabled then Exit;
  AddFavoriteLangTag(FLangTagIntern);

  // Normalization once
  if FNormalizationOnce = nfCanonical then
  FLanguageTag:=FCanonicalForm
  else
  if FNormalizationOnce = nfExtLang then
  FLanguageTag:=FExtLangForm
  else
  // Normalization auto
  if FNormalizationAuto = nfCanonical then
  FLanguageTag:=FCanonicalForm
  else
  if FNormalizationAuto = nfExtLang then
  FLanguageTag:=FExtLangForm
  else
  // no normalization
  FLanguageTag:=FLangTagIntern;

  FNormalizationOnce:=nfNone;
  Close;
end;

// Action - Cancel language edit - close form
procedure TFormBCP47.Act_CancelExecute(Sender: TObject);
begin
  Close;
end;




(* Free-form input *)

// Cob change input
procedure TFormBCP47.CoB_FreeInputChange(Sender: TObject);
begin
  Validate(true);
end;


(* Language *)

// change language
procedure TFormBCP47.CoB_LanguageChange(Sender: TObject);
begin
  with CoB_Language do
  FLangBCP47.LanguageData:=PLanguage(Items.Objects[ItemIndex]);
  Validate();
end;




// set the language CoB
procedure TFormBCP47.Set_CoB_Language;
var
  i: Integer;
  lng: PLanguage;
  cap: String;
begin
  with CoB_Language do
  if FLangBCP47.Language = '' then
  begin
    ItemIndex:=0;
    if Tag = 1 then
    begin
      Items.Delete(1);
      Tag:=0;
    end;
  end
  else
  with Items do
  begin
    for i:=1 to Count -1 do
    if PLanguage(Objects[i])^.CodeMatch(FLangBCP47.Language) then
    begin
      ItemIndex:=i;
      if (Tag = 1) and (i > 1) then
      begin
        Delete(1);
        Tag:=0;
      end;
      Exit;
    end;
    // not found, add a new entry at position 1 or use the exists one
    lng:=Find_Language(FLangBCP47.Language);
    cap:=lng^.Get_Caption(FCodesAtFirstPlace);
    ItemWidth:=Max(Canvas.TextWidth(cap)+8, ItemWidth);

    if Tag = 0 then
    begin
      InsertObject(1, cap, TObject(lng));
      Tag:=1;
    end
    else
    begin
      Strings[1]:=cap;
      Objects[1]:=TObject(lng);
    end;

    ItemIndex:=1;
  end;
end;




(* Extended language *)

// change Extended language
procedure TFormBCP47.CoB_ExtLangChange(Sender: TObject);
begin
  with CoB_ExtLang do
  if ItemIndex > 0 then
  FLangBCP47.ExtLang:=PExtLang(Items.Objects[ItemIndex])^.Alpha_3_Code
  else FLangBCP47.ExtLang:='';
  Validate();
end;

// set the Extended language CoB
procedure TFormBCP47.Set_CoB_ExtLangs;
var
  i: Integer;
begin
  with CoB_ExtLang do
  if FLangBCP47.ExtLang = '' then
  ItemIndex:=0
  else
  with Items do
  begin
    for i:=1 to Count -1 do
    if PExtLang(Objects[i])^.Alpha_3_Code = FLangBCP47.ExtLang then
    begin
      ItemIndex:=i;
      Exit;
    end;
  end;
end;



(* Script *)

// onChange Script
procedure TFormBCP47.CoB_ScriptChange(Sender: TObject);
var
  s: String;
begin
  with CoB_Script do
  if ItemIndex = 0 then
  s:=''
  else s:=PScript(Items.Objects[ItemIndex])^.Alpha_4_Code;

  FLangBCP47.Script:=s;

  Validate();
end;

// set the Script CoB
procedure TFormBCP47.Set_CoB_Script;
var
  i: Integer;
begin
  with CoB_Script do
  if FLangBCP47.Script = '' then
  ItemIndex:=0
  else
  with Items do
  begin
    for i:=1 to Count -1 do
    if PScript(Objects[i])^.Alpha_4_Code = FLangBCP47.Script then
    begin
      ItemIndex:=i;
      Exit;
    end;
  end;
end;



(* Region *)

// onChange Region
procedure TFormBCP47.CoB_RegionChange(Sender: TObject);
var
  reg: String;
begin
  with CoB_Region do
  if ItemIndex = 0 then
  reg:=''
  else reg:=PRegion(Items.Objects[ItemIndex])^.Get_Code;

  FLangBCP47.Region:=reg;

  Validate();
end;

// set the Region CoB
procedure TFormBCP47.Set_CoB_Region;
var
  i: Integer;
  reg: PRegion;
  cap: String;
begin
  with CoB_Region do
  if FLangBCP47.Region = '' then
  begin
    ItemIndex:=0;
    if Tag = 1 then
    begin
      Items.Delete(1);
      Tag:=0;
    end;
  end
  else
  with Items do
  begin
    for i:=1 to Count -1 do
    if PRegion(Objects[i])^.CodeMatch(FLangBCP47.Region) then
    begin
      ItemIndex:=i;
      if (Tag = 1) and (i > 1) then
      begin
        Items.Delete(1);
        Tag:=0;
      end;
      Exit;
    end;
    // not found, add a new entry at position 1 or use the exists one
    reg:=Find_Region(FLangBCP47.Region);
    cap:=reg^.Get_Caption(FCodesAtFirstPlace);
    ItemWidth:=Max(Canvas.TextWidth(cap)+8, ItemWidth);

    if Tag = 0 then
    begin
      InsertObject(1, cap, TObject(reg));
      Tag:=1;
    end
    else
    begin
      Strings[1]:=cap;
      Objects[1]:=TObject(reg);
    end;

    ItemIndex:=1;
  end;
end;






(* Variants *)

// add Variant
procedure TFormBCP47.SpB_VariantsAddClick(Sender: TObject);
var
  new_comps: TNewComps;
begin
  new_comps:=TNewComps.Create;

  new_comps.aInput:=TComboBox.Create(Pan_Variants);
  with TComboBox(new_comps.aInput) do
  begin
    DropDownCount:=CoB_Variants.DropDownCount;
    Style:=csDropDownList;

    if FVariants.Count = 1 then
    Set_Anchors(new_comps.aInput, CoB_Variants)
    else
    Set_Anchors(new_comps.aInput,
    TNewComps(FVariants.Objects[FVariants.Count -1]).aInput);

    Color:=clBtnFace;
    Parent:=Pan_Variants;
    Items:=CoB_Variants.Items;
    if CoB_Variants.Tag = 1 then
    Items.Delete(1);
    ItemWidth:=CoB_Variants.ItemWidth;
    ItemIndex:=0;
    OnChange:=@CoB_VariantsChange;
  end;

  new_comps.aButton:=TSpeedButton.Create(Pan_Variants);
  with TSpeedButton(new_comps.aButton) do
  begin
    Height:=24;
    Width:=24;
    Set_Anchors_Button(new_comps.aButton, new_comps.aInput);
    ImL_16.GetBitmap(iiDel, Glyph);
    Parent:=Pan_Variants;
    OnClick:=@SpB_VariantDelClick;
  end;

  FVariants.AddObject('', new_comps);
  if FVariants.Count = maxVariants then
  SpB_VariantsAdd.Enabled:=false;
end;

// delete Variant
procedure TFormBCP47.SpB_VariantDelClick(Sender: TObject);
var
  i: Byte;
begin
  with FVariants do
  for i:=Count -1 downto 1 do
  if TNewComps(Objects[i]).aButton = Sender then
  begin
    if i < (Count -1) then
    if i = 1 then
    Set_Anchors(TNewComps(Objects[i+1]).aInput, CoB_Variants)
    else
    Set_Anchors(TNewComps(Objects[i+1]).aInput, TNewComps(Objects[i-1]).aInput);

    Delete(i);
    Break;
  end;

  SpB_VariantsAdd.Enabled:=true;

  FLangBCP47.SetVariants(FVariants);
  Validate;
end;

// change Variant
procedure TFormBCP47.CoB_VariantsChange(Sender: TObject);
var
  i,idx: Integer;
begin
  idx:=TComboBox(Sender).ItemIndex;

  if Sender = CoB_Variants then
  begin
    if idx = 0 then FVariants[0]:=''
    else FVariants[0]:=PVariant(CoB_Variants.Items.Objects[idx])^.Code;
  end
  else
  for i:=1 to FVariants.Count -1 do
  with TNewComps(FVariants.Objects[i]) do
  if Sender = aInput then
  begin
    if idx = 0 then FVariants[i]:=''
    else FVariants[i]:=PVariant(TComboBox(aInput).Items.Objects[idx])^.Code;
    Break;
  end;

  FLangBCP47.Variants:=FVariants; //SetVariants(FVariants);
  Validate();
end;

// set the Variants Cob
procedure TFormBCP47.Set_CoB_Variants;
var
  i, new_count, old_count: Integer;
  cap: String;

  procedure Set_Cob(const aCoB: TComboBox; const Vidx: Byte);
  var
    idx: Integer;
  begin
    FVariants[Vidx]:=FLangBCP47.Variants[Vidx];

    with aCoB do
    begin
      idx:=Items.IndexOfObject(FLangBCP47.Variants.Objects[Vidx]);
      if idx = -1 then
      begin
        if Tag = 0 then
        begin
          cap:=PVariant(FLangBCP47.Variants.Objects[Vidx])
          ^.Get_Caption(FCodesAtFirstPlace);
          Items.InsertObject(1, cap , FLangBCP47.Variants.Objects[Vidx]);
          Tag:=1;
        end
        else
        begin
          Items[1]:=cap;
          Items.Objects[1]:=FLangBCP47.Variants.Objects[Vidx];
        end;
        ItemIndex:=1;
      end
      else
      begin
        ItemIndex:=idx;
        if (Tag = 1) and (idx > 1) then
        begin
          Items.Delete(1);
          Tag:=0;
        end;
      end;
    end;
  end;

begin
  // check the number of CoB
  new_count:=FLangBCP47.Variants.Count;
  old_count:=FVariants.Count;
  SpB_VariantsAdd.Enabled:=new_count < maxVariants;

  if new_count <> old_count then
  begin
    if new_count = 0 then
    begin
      if old_count > 1 then
      for i:=old_count -1 downto 1 do
      FVariants.Delete(i);
      FVariants[0]:='';
      CoB_Variants.ItemIndex:=0;
      Exit;
    end;

    if new_count > old_count then                // add new Variant CoB
    for i:=1 to (new_count - old_count) do
    SpB_VariantsAddClick(nil)
    else                                         // delete Variant CoB
    for i:=old_count -1 downto new_count do
    FVariants.Delete(i);
  end;

  Set_Cob(CoB_Variants, 0);

  if new_count > 1 then
  for i:=1 to new_count -1 do
  Set_Cob(TComboBox(TNewComps(FVariants.Objects[i]).aInput), i);
end;


(* Extensions *)

// Add Extensions components
procedure TFormBCP47.SpB_ExtensionsAddClick(Sender: TObject);
var
  new_comps: TNewComps2;
begin
  new_comps:=TNewComps2.Create;

  new_comps.aInput:=TComboBox.Create(Pan_Extensions);
  with TComboBox(new_comps.aInput) do
  begin
    if FExtensions.Count = 1 then
    Set_Anchors(new_comps.aInput, CoB_Extensions)
    else
    Set_Anchors(new_comps.aInput,
    TNewComps2(FExtensions.Objects[FExtensions.Count -1]).aInput);

    DropDownCount:=CoB_Extensions.DropDownCount;
    Style:=csDropDownList;
    Color:=clBtnFace;
    Parent:=Pan_Extensions;
    Items:=CoB_Extensions.Items;
    ItemIndex:=0;
    ItemWidth:=CoB_Extensions.ItemWidth;
    OnChange:=@ExtensionsChange;
  end;

  new_comps.aInput2:=TEdit.Create(Pan_Extensions);
  with TEdit(new_comps.aInput2) do
  begin
    if FExtensions.Count = 1 then
    Set_Anchors(new_comps.aInput2, Edt_Extensions)
    else
    Set_Anchors(new_comps.aInput2,
    TNewComps2(FExtensions.Objects[FExtensions.Count -1]).aInput2);

    Parent:=Pan_Extensions;
    OnChange:=@ExtensionsChange;
  end;

  new_comps.aButton:=TSpeedButton.Create(Pan_Extensions);
  with TSpeedButton(new_comps.aButton) do
  begin
    Set_Anchors_Button(new_comps.aButton, new_comps.aInput2);
    ImL_16.GetBitmap(iiDel, Glyph);
    Parent:=Pan_Extensions;
    OnClick:=@SpB_ExtensionsDelClick;
  end;

  FExtensions.AddObject('', new_comps);
  if FExtensions.Count = maxExtensions then
  SpB_ExtensionsAdd.Enabled:=false;
end;

// delete Extensions
procedure TFormBCP47.SpB_ExtensionsDelClick(Sender: TObject);
var
  i: Byte;
begin
  with FExtensions do
  for i:=Count -1 downto 1 do
  if TNewComps2(Objects[i]).aButton = Sender then
  begin
    if i < (Count -1) then
    if i = 1 then
    Set_Anchors(TNewComps2(Objects[i+1]).aInput2, Edt_Extensions)
    else
    Set_Anchors(TNewComps2(Objects[i+1]).aInput2,
    TNewComps2(Objects[i-1]).aInput2);

    Delete(i);
    Break;
  end;

  SpB_ExtensionsAdd.Enabled:=true;

  FLangBCP47.SetExtensions(FExtensions);
  Validate;
end;

// change Extensions
procedure TFormBCP47.ExtensionsChange(Sender: TObject);
var
  i, idx: Integer;
  ext: String = '';
begin
  if FUpdate then Exit;
  if (Sender = CoB_Extensions) or (Sender = Edt_Extensions) then
  begin
    idx:=CoB_Extensions.ItemIndex;
    if (idx = 0) or (Edt_Extensions.Text = '') then FExtensions[0]:=''
    else
    begin
      ext:=PExtensions(CoB_Extensions.Items.Objects[idx])^.Identifier;
      FExtensions[0]:=ext + '-' + Edt_Extensions.Text;
    end;
  end
  else
  for i:=1 to FExtensions.Count -1 do
  with TNewComps2(FExtensions.Objects[i]) do
  if (Sender = aInput) or (Sender = aInput2) then
  begin
    idx:=TComboBox(aInput).ItemIndex;
    if (idx = 0) or (TEdit(aInput2).Text = '') then FExtensions[i]:=''
    else
    begin
      ext:=PExtensions(CoB_Extensions.Items.Objects[idx])^.Identifier;
      FExtensions[i]:=ext + '-' + TEdit(aInput2).Text;
    end;
    Break;
  end;

  FLangBCP47.Extensions:=FExtensions;
  Validate();
end;

// set the comps (CoB and Edit) for the extensions
procedure TFormBCP47.Set_Comps_Extensions;
var
  i, new_count, old_count: Integer;

  procedure Set_Comps(const aCoB: TComboBox; const aEdt: TEdit;
            const Eidx: Byte);
  var
    idx: Integer;
    ident: Char;
  begin
    FExtensions[Eidx]:=FLangBCP47.Extensions[Eidx];
    if FExtensions[Eidx] = '' then
    begin
      aCoB.ItemIndex:=0;
      aEdt.Text:='';
      Exit;
    end;

    aEdt.Text:=copy(FExtensions[Eidx], 3, MaxInt);
    ident:=FExtensions[Eidx][1];
    with aCoB do
    for idx:=1 to Items.Count -1 do
    if ident = PExtensions(Items.Objects[idx])^.Identifier then
    begin
      ItemIndex:=idx;
      Exit;
    end;
  end;

begin
  // check the number of comps
  new_count:=FLangBCP47.Extensions.Count;
  old_count:=FExtensions.Count; 
  SpB_ExtensionsAdd.Enabled:=new_count < maxExtensions;

  if new_count <> old_count then
  begin
    if new_count = 0 then
    begin
      if old_count > 1 then
      for i:=old_count -1 downto 1 do
      FExtensions.Delete(i);
      FExtensions[0]:='';
      CoB_Extensions.ItemIndex:=0;
      Edt_Extensions.Text:='';
      Exit;
    end;

    if new_count > old_count then                // add new Extension comps
    for i:=1 to (new_count - old_count) do
    SpB_ExtensionsAddClick(nil)
    else                                         // delete Extension comps
    for i:=old_count -1 downto new_count do
    FExtensions.Delete(i);
  end;
  Set_Comps(CoB_Extensions, Edt_Extensions, 0);

  if new_count > 1 then
  for i:=1 to new_count -1 do
  with TNewComps2(FExtensions.Objects[i]) do
  Set_Comps(TComboBox(aInput), TEdit(aInput2), i);
end;


(* Private use *)

// Add Private use
procedure TFormBCP47.SpB_PrivateUseAddClick(Sender: TObject);
var
  new_comps: TNewComps;
begin
  new_comps:=TNewComps.Create;

  new_comps.aInput:=TEdit.Create(Pan_PrivateUse);
  with TEdit(new_comps.aInput) do
  begin
    if FPrivateUses.Count = 1 then
    Set_Anchors(new_comps.aInput, Edt_PrivateUse)
    else
    Set_Anchors(new_comps.aInput,
    TNewComps(FPrivateUses.Objects[FPrivateUses.Count -1]).aInput);

    Parent:=Pan_PrivateUse;
    OnChange:=@Edt_PrivateUseChange;
  end;

  new_comps.aButton:=TSpeedButton.Create(Pan_PrivateUse);
  with TSpeedButton(new_comps.aButton) do
  begin
    Height:=24;
    Width:=24;
    Set_Anchors_Button(new_comps.aButton, new_comps.aInput);
    ImL_16.GetBitmap(iiDel, Glyph);
    Parent:=Pan_PrivateUse;
    OnClick:=@SpB_PrivteUseDelClick;
  end;

  FPrivateUses.AddObject('', new_comps);
  if FPrivateUses.Count = maxPrivateUse then
  SpB_PrivateUseAdd.Enabled:=false;
end;

// delete Private use
procedure TFormBCP47.SpB_PrivteUseDelClick(Sender: TObject);
var
  i: Byte;
begin
  with FPrivateUses do
  for i:=Count -1 downto 1 do
  if TNewComps(Objects[i]).aButton = Sender then
  begin
    if i < (Count -1) then
    if i = 1 then
    Set_Anchors(TNewComps(Objects[i+1]).aInput, Edt_PrivateUse)
    else
    Set_Anchors(TNewComps(Objects[i+1]).aInput, TNewComps(Objects[i-1]).aInput);

    Delete(i);
    Break;
  end;

  FLangBCP47.PrivateUses:=FPrivateUses;
  Validate;
end;

// change Private use
procedure TFormBCP47.Edt_PrivateUseChange(Sender: TObject);
var
  i: Integer;
begin
  if FUpdate then Exit;
  if Sender = Edt_PrivateUse then
  FPrivateUses[0]:=Edt_PrivateUse.Text
  else
  for i:=1 to FPrivateUses.Count -1 do
  with TNewComps(FPrivateUses.Objects[i]) do
  if Sender = aInput then
  begin
    FPrivateUses[i]:=TEdit(aInput).Text;
    Break;
  end;

  FLangBCP47.PrivateUses:=FPrivateUses;
  Validate();
end;

// set the Private use Edits
procedure TFormBCP47.Set_Edt_PrivateUse;
var
  i, new_count, old_count: Integer;
begin
  // check the number of edits
  new_count:=FLangBCP47.PrivateUses.Count;
  old_count:=FPrivateUses.Count;
  SpB_PrivateUseAdd.Enabled:=new_count < maxPrivateUse;

  if new_count <> old_count then
  begin
    if new_count = 0 then
    begin
      if old_count > 1 then
      for i:=old_count -1 downto 1 do
      FPrivateUses.Delete(i);
      FPrivateUses[0]:='';
      Edt_PrivateUse.Text:='';
      Exit;
    end;

    if new_count > old_count then                // add new Private use edits
    for i:=1 to (new_count - old_count) do
    SpB_PrivateUseAddClick(nil)
    else                                         // delete Private use edits
    for i:=old_count -1 downto new_count do
    FPrivateUses.Delete(i);
  end;

  FPrivateUses[0]:=FLangBCP47.PrivateUses[0];
  Edt_PrivateUse.Text:=FPrivateUses[0];

  if new_count > 1 then
  for i:=1 to new_count -1 do
  begin
    FPrivateUses[i]:=FLangBCP47.PrivateUses[i];
    TEdit(TNewComps(FPrivateUses.Objects[i]).aInput).Text:=FPrivateUses[i];
  end;
end;



(* Grandfathered *)

// change Grandfathered
procedure TFormBCP47.CoB_GrandfatheredChange(Sender: TObject);
begin
  with CoB_Grandfathered do
  if ItemIndex = 0 then
  FLangBCP47.Grandfathered:=''
  else
  FLangBCP47.Grandfathered:=PGrandfathered(Items.Objects[ItemIndex])^.Tag;
  Validate();
end;

// set the Grandfathered CoB
procedure TFormBCP47.Set_CoB_Grandfathered;
var
  i: Integer;
  gf: PGrandfathered;
  cap: String;
begin
  with CoB_Grandfathered do
  if FLangBCP47.Grandfathered = '' then
  begin
    ItemIndex:=0;
    if Tag = 1 then
    begin
      Items.Delete(1);
      Tag:=0;
    end;
  end
  else
  with Items do
  begin
    for i:=1 to Count -1 do
    if PGrandfathered(Objects[i])^.Tag = FLangBCP47.Grandfathered then
    begin
      ItemIndex:=i;
      if (i > 1) and (Tag = 1) then
      begin
        Delete(1);
        Tag:=0;
      end;
      Exit;
    end;
    // not found, add a new entry at position 1 or use the exists one
    gf:=Find_Grandfathered(FLangBCP47.Grandfathered);
    cap:=gf^.Get_Caption(FCodesAtFirstPlace);
    ItemWidth:=Max(Canvas.TextWidth(cap)+8, ItemWidth);

    if Tag = 0 then
    begin
      InsertObject(1, cap, TObject(gf));
      Tag:=1;
    end
    else
    begin
      Strings[1]:=cap;
      Objects[1]:=TObject(gf);
    end;

    ItemIndex:=1;
  end;
end;




(* Normalization *)

// SpB onClick
procedure TFormBCP47.SpB_NormalizationClick(Sender: TObject);
begin
  with SpB_Normalization do
  if Tag = 1 then
  begin
    Tag:=0;
    Down:=false;
  end
  else
  if Down then
  with ClientToScreen(point(0, Height)) do
  PoM_Normalization.Popup(X, Y);
end;

// Popup onClose
procedure TFormBCP47.PoM_NormalizationClose(Sender: TObject);
begin
  SpB_Normalization.Down:=false;
  if FindControlAtPosition(Mouse.CursorPos, true) = SpB_Normalization then
  SpB_Normalization.Tag:=1;
end;


// Popup menu item onClick - auto option
procedure TFormBCP47.Mit_Normalization_AutoClick(Sender: TObject);
begin                        
  FNormalizationOnce:=nfNone;
  TMenuItem(Sender).Checked:=true;
  if Sender = Mit_NoNormalization then
  FNormalizationAuto:=nfNone
  else
  begin
    if Sender = Mit_CanonicalAuto then
    begin
      FNormalizationAuto:=nfCanonical;
      CoB_FreeInput.Text:=FCanonicalForm;
    end
    else
    if Sender = Mit_ExtLangAuto then
    begin
      FNormalizationAuto:=nfExtLang;
      CoB_FreeInput.Text:=FExtLangForm;
    end;
    Validate(true);
  end;
end;

// Popup menu item onClick - once option
procedure TFormBCP47.Mit_Normalization_OnceClick(Sender: TObject);
begin                              
  if Sender = Mit_CanonicalOnce then
  begin
    FNormalizationOnce:=nfCanonical;
    CoB_FreeInput.Text:=FCanonicalForm;
  end
  else
  if Sender = Mit_ExtLangOnce then
  begin
    FNormalizationOnce:=nfExtLang;
    CoB_FreeInput.Text:=FExtLangForm;
  end;
  Validate(true);
end;




// Init language
procedure TFormBCP47.InitLanguage_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_Language do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  if (not FLanguagesOftenOnly) or (FLanguagesOftenUsed = nil)
  or (FLanguagesOftenUsed.Count = 0) then
  begin
    for i:=0 to High(LanguageArray) do
    begin
      if (not FDeprecatedAllowed) and LanguageArray[i].Deprecated then
      Continue;

      if (not FUse_ISO639_3_and_5)
      and (not LanguageArray[i].Is_Part_of_ISO639_2) then
      Continue;

      cap:=LanguageArray[i].Get_Caption(FCodesAtFirstPlace);
      newW:=Max(Canvas.TextWidth(cap)+8, newW);
      CoB_Language.AddItem(cap, TObject(@LanguageArray[i]));
    end;

    with CoB_Language do
    begin
      Sorted:=true;
      Sorted:=false;
      i:=Items.Count -1;
      if not FCodesAtFirstPlace then
      while Items[1][1] <> 'A' do
      Items.Move(1, i);
    end;
  end;

  if FLanguagesOftenUsed <> nil then with FLanguagesOftenUsed do
  for i:=0 to Count -1 do
  begin
    cap:=PLanguage(Objects[i])^.Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_Language.Items.InsertObject(i+1, cap, Objects[i]);
  end;

  with CoB_Language do
  begin
    if Items.Count > DropDownCount then
    Inc(newW, 24);
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;

// Init Extended language
procedure TFormBCP47.InitExtLang_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_ExtLang do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  for i:=0 to High(ExtLangArray) do
  begin
    if (not FDeprecatedAllowed) and ExtLangArray[i].Deprecated then
    Continue;

    cap:=ExtLangArray[i].Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_ExtLang.AddItem(cap, TObject(@ExtLangArray[i]));
  end;

  with CoB_ExtLang do
  begin
    Sorted:=true;
    Sorted:=false;
    if Items.Count > DropDownCount then
    Inc(newW, 24);
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;

// Init Script
procedure TFormBCP47.InitScript_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_Script do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  for i:=0 to High(ScriptArray) do
  begin
    cap:=ScriptArray[i].Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_Script.AddItem(cap, TObject(@ScriptArray[i]));
  end;

  with CoB_Script do
  begin
    Sorted:=true;
    Sorted:=false;
    //if Items.Count > DropDownCount then
    Inc(newW, 24);
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;

// Init Region
procedure TFormBCP47.InitRegion_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_Region do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  if (not FRegionsOftenOnly) or (FRegionsOftenUsed = nil)
  or (FRegionsOftenUsed.Count = 0) then
  begin
    for i:=0 to High(RegionArray) do
    begin
      if (not FDeprecatedAllowed) and RegionArray[i].Deprecated then
      Continue;

      cap:=RegionArray[i].Get_Caption(FCodesAtFirstPlace);
      newW:=Max(Canvas.TextWidth(cap)+8, newW);
      CoB_Region.AddItem(cap, TObject(@RegionArray[i]));
    end;

    with CoB_Region do
    begin
      Sorted:=true;
      Sorted:=false;
    end;
  end;

  if FRegionsOftenUsed <> nil then with FRegionsOftenUsed do
  for i:=0 to Count -1 do
  begin
    cap:=PRegion(Objects[i])^.Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_Region.Items.InsertObject(i+1, cap, Objects[i]);
  end;

  with CoB_Region do
  begin
    if Items.Count > DropDownCount then
    Inc(newW, 24);
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;

// Init Variants
procedure TFormBCP47.InitVariant_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_Variants do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  for i:=0 to High(VariantArray) do
  begin
    if (not FDeprecatedAllowed) and VariantArray[i].Deprecated then
    Continue;

    cap:=VariantArray[i].Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_Variants.AddItem(cap, TObject(@VariantArray[i]));
  end;

  with CoB_Variants do
  begin
    Sorted:=true;
    Sorted:=false;
    if Items.Count > DropDownCount then
    Inc(newW, 24);
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;

// Init Extensions
procedure TFormBCP47.InitExtensions_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_Extensions do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  for i:=0 to High(ExtensionsArray) do
  begin
    cap:=ExtensionsArray[i].Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_Extensions.AddItem(cap, TObject(@ExtensionsArray[i]));
  end;

  with CoB_Extensions do
  begin
    Sorted:=true;
    Sorted:=false;
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;

// Init Grandfathered
procedure TFormBCP47.InitGrandfathered_Cob;
var
  i: Integer; newW: Integer=0;
  cap: String;
begin
  with CoB_Grandfathered do
  begin
    if Items.Count > 0 then Clear;
    Items.Add('');
  end;

  for i:=0 to High(GrandfatheredArray) do
  begin
    if (not FDeprecatedAllowed) and GrandfatheredArray[i].Deprecated then
    Continue;

    cap:=GrandfatheredArray[i].Get_Caption(FCodesAtFirstPlace);
    newW:=Max(Canvas.TextWidth(cap)+8, newW);
    CoB_Grandfathered.AddItem(cap, TObject(@GrandfatheredArray[i]));
  end;

  with CoB_Grandfathered do
  begin
    Sorted:=true;
    Sorted:=false;
    //if Items.Count > DropDownCount then
    //Inc(newW, 24);
    ItemWidth:=newW;
    ItemIndex:=0;
  end;
end;
  
// Init NormalizationAuto
procedure TFormBCP47.InitNormalizationAuto;
begin
  case FNormalizationAuto of
    nfCanonical: Mit_CanonicalAuto.Checked:=true;
    nfExtLang  : Mit_ExtLangAuto.Checked:=true;
    nfNone     : Mit_NoNormalization.Checked:=true;
  end;
end;


// set the comps from a valid BCP47 language tag
procedure TFormBCP47.SetComps;
begin
  FUpdate:=true;
  Set_CoB_Language;
  Set_CoB_ExtLangs;
  Set_CoB_Script;
  Set_CoB_Region;
  Set_CoB_Variants;
  Set_Comps_Extensions;
  Set_Edt_PrivateUse;
  Set_CoB_Grandfathered;
  FUpdate:=false;
end;

// translate CoB Items
procedure TFormBCP47.Translate_CoBItems;
var
  i: Integer;
begin
  (* for the moment only the "Undetermined" caption in the Language CoB
     needs a translation
  *)
  with CoB_Language do
  for i:=1 to Items.Count -1 do
  with PLanguage(Items.Objects[i])^ do
  if CodeMatch('und') then
  begin
    Items[i]:=Get_Caption(FCodesAtFirstPlace);
    ItemWidth:=Max(Canvas.TextWidth(Items[i])+8, ItemWidth);
    Exit;
  end;
end;



// Validate language
function TFormBCP47.Validate(const isFreeFormInput: Boolean): Boolean;
var
  langTag: String;
  bcp47: TLanguageBCP47;
  freeBCP47: Boolean = true;

  procedure SetComps_for_FreeFormInput;
  begin
    if not isFreeFormInput then Exit;
    FreeAndNil(FLangBCP47);
    FLangBCP47:=bcp47;
    SetComps;
    freeBCP47:=false;
  end;

begin
  Result:=false;
  if isFreeFormInput then
  langTag:=CoB_FreeInput.Text
  else
  begin
    langTag:=FLangBCP47.LanguageTag;
    CoB_FreeInput.Text:=langTag;
  end;

  bcp47:=TLanguageBCP47.Parse(langTag);
  SpB_OK.Enabled:=bcp47.Is_Valid;

  if bcp47.Is_Valid then
  begin
    FLangTagIntern:=bcp47.LanguageTag;
    FCanonicalForm:=bcp47.CanonicalForm;
    FExtLangForm:=bcp47.ExtLangSubtagsForm;
    SetComps_for_FreeFormInput;
  end
  else
  begin
    if langTag = '' then SetComps_for_FreeFormInput;
    FLangTagIntern:='';
    FCanonicalForm:='';
    FExtLangForm:='';
  end;
  SetStatusInfo(bcp47);
  SetNormalizationItems;

  if freeBCP47 then
  bcp47.Free else bcp47:=nil;
end;


// set status info
procedure TFormBCP47.SetStatusInfo(const aBCP: TLanguageBCP47);
var
  left_offset: Word;

  procedure Create_Comps(const aText: String; const st: TStatusType;
            const aTypeCaption:  String = '');
  var
    statsComps: TStatusComps;
  begin
    statsComps:=TStatusComps.Create;
    FStatusComps.Add(statsComps);

    (* Text label *)
    statsComps.Lbl_StatusText:=TLabel.Create(Pan_Status);
    with statsComps.Lbl_StatusText do
    begin
      if FStatusComps.Count = 1 then
      AnchorSideTop.Control:=Lbl_StatusInfo
      else
      AnchorSideTop.Control:=TStatusComps
      (FStatusComps.Items[FStatusComps.Count-2]).Lbl_StatusText;
      AnchorSideTop.Side:=asrBottom;
      AnchorSideLeft.Control:=Lbl_StatusInfo;
      AnchorSideLeft.Side:=asrLeft;
      AnchorSideRight.Control:=Lbl_StatusInfo;
      AnchorSideRight.Side:=asrRight;
      Anchors := [akLeft, akRight, akTop];
      BorderSpacing.Bottom:=10;
      Parent:=Pan_Status;
      WordWrap:=true;
      Caption:=aText;
    end;

    (* Image *)
    statsComps.Img_StatusType:=TImage.Create(Pan_Status);
    with statsComps, Img_StatusType do
    begin
      Height:=16; Width:=16;
      AnchorSideLeft.Control := Img_Status;
      AnchorSideLeft.Side := asrLeft;
      AnchorSideTop.Control:=Lbl_StatusText;
      AnchorSideTop.Side:=asrCenter;
      Parent:=Pan_Status;
      case st of
        stOK    : ImL_16.GetBitmap(iiOK,    Picture.Bitmap);
        stWarnig: ImL_16.GetBitmap(iiWarn,  Picture.Bitmap);
        stError : ImL_16.GetBitmap(iiError, Picture.Bitmap);
        stInfo  : ImL_16.GetBitmap(iiInfo,  Picture.Bitmap);
      end;
    end;

    (* Type caption label *)
    if aTypeCaption <> '' then
    begin
      statsComps.Lbl_StatusType:=TLabel.Create(Pan_Status);
      with statsComps, Lbl_StatusType do
      begin
        AnchorSideTop.Control := Img_StatusType;
        AnchorSideTop.Side := asrCenter;
        AnchorSideLeft.Control := Lbl_Status;
        AnchorSideLeft.Side := asrLeft;
        Anchors := [akLeft, akTop];
        Parent:=Pan_Status;
        Caption:=aTypeCaption;
        if Width > left_offset then
        left_offset:=Width;
      end;
    end
    else
    statsComps.Lbl_StatusType:=nil;
  end;

  procedure Process_Warnings;
  var
    i: Integer;
  begin
    Create_Comps(aBCP.Warnings[0], stWarnig, rsLbl_Warnings);
    for i:=1 to aBCP.Warnings.Count -1 do
    Create_Comps(aBCP.Warnings[i], stWarnig);
  end;

begin
  Pan_Status.AutoSize:=false;
  left_offset:=Lbl_Status.Width;

  with FStatusComps do while Count > 0 do
  begin
    TStatusComps(Items[Count -1]).Free;
    Delete(Count -1);
  end;

  (* valid *)
  if aBCP.Is_Valid then
  begin
    Lbl_StatusInfo.Caption:=rsValidLanguageTag;
    ImL_16.GetBitmap(iiOK,  Img_Status.Picture.Bitmap);

    (* information *)
    if aBCP.InfoText <> '' then
    Create_Comps(aBCP.InfoText, stInfo, rsLbl_Infomation);

    (* warnings *)
    if aBCP.Warnings.Count > 0 then
    Process_Warnings;

  end else
  (* invalid *)
  begin
    Lbl_StatusInfo.Caption:=rsNoValidLanguageTag;
    ImL_16.GetBitmap(iiError, Img_Status.Picture.Bitmap);
    Create_Comps(aBCP.ErrorText, stError, rsLbl_ParserError);
  end;

  // set position of text labels
  Img_Status.BorderSpacing.Left:=left_offset - Lbl_Status.Width + 8;

  Pan_Status.AutoSize:=true;
end;


// set normalization items
procedure TFormBCP47.SetNormalizationItems;
begin
  Mit_CanonicalOnce.Enabled:=LowerCase(FLangTagIntern)
  <> LowerCase(FCanonicalForm);
  Mit_ExtLangOnce.Enabled:=LowerCase(FLangTagIntern) <> LowerCase(FExtLangForm);
end;



// add a favorite language tag to the free-input combobox
procedure TFormBCP47.AddFavoriteLangTag(const ALangTag: String);
begin
  with CoB_FreeInput.Items do
  if IndexOf(ALangTag) = -1 then
  Add(ALangTag);
end;


// DeprecatedAllowed has changed
procedure TFormBCP47.DeprecatedAllowed_Changed;
begin
  // change some components entries
  InitLanguage_Cob;
  InitExtLang_Cob;

  InitRegion_Cob;
  InitVariant_Cob; 
  with FVariants do
  while Count > 1 do
  Delete(Count -1);

  InitGrandfathered_Cob;
end;

// CodesAtFirstPlace has changed
procedure TFormBCP47.CodesAtFirstPlace_Changed;
begin
  // we can use the DeprecatedAllowed_Changed method and here the rest
  DeprecatedAllowed_Changed;
  InitScript_Cob;
  InitExtensions_Cob;
  with FExtensions do
  while Count > 1 do
  Delete(Count -1);
end;

// set the position of some components
procedure TFormBCP47.Set_CompsPosition;
var
  minLeft: Integer;
  m: Integer = 0;
begin
  m:=Max(Lbl_Language.Width, m);
  m:=Max(Lbl_ExtLang.Width, m);
  m:=Max(Lbl_Script.Width, m);
  m:=Max(Lbl_Region.Width, m);
  m:=Max(Lbl_Variants.Width, m);
  m:=Max(Lbl_Extensions.Width, m);
  m:=Max(Lbl_PrivateUse.Width, m);
  m:=Max(Lbl_Grandfathered.Width, m);

  minLeft:=m - Lbl_ExtLang.Width + 15;
  CoB_ExtLang.BorderSpacing.Left:=minLeft;

  CoB_Variants.Left:=CoB_ExtLang.Left;
  CoB_Extensions.Left:=CoB_ExtLang.Left;
  Edt_PrivateUse.Left:=CoB_ExtLang.Left;
end;

// change font values of the form
procedure TFormBCP47.Change_FontValues;
var
  l: Integer;
begin
  FNewFontSize:=Font.Size;
  FNewFontName:=Font.Name;

  Set_CompsPosition;

  Set_CobItemWidth(CoB_Language);
  Set_CobItemWidth(CoB_ExtLang);
  Set_CobItemWidth(CoB_Script);
  Set_CobItemWidth(CoB_Region);
  Set_CobItemWidth(CoB_Variants);
  Set_CobItemWidth(CoB_Extensions);
  Set_CobItemWidth(CoB_Grandfathered);

  with FVariants do for l:=1 to Count -1 do
  //Set_CobItemWidth(TComboBox(TNewComps(Objects[l]).aInput));
  TComboBox(TNewComps(Objects[l]).aInput).ItemWidth:=CoB_Variants.ItemWidth;

  with FExtensions do for l:=1 to Count -1 do
  //Set_CobItemWidth(TComboBox(TNewComps(Objects[l]).aInput));
  TComboBox(TNewComps(Objects[l]).aInput).ItemWidth:=CoB_Extensions.ItemWidth;

  Change_CompsWidth;
end;

// change components width
procedure TFormBCP47.Change_CompsWidth;
begin
  (* SpB_Normalization *)
  with SpB_Normalization do
  Constraints.MinWidth := Canvas.GetTextWidth(Text) + 20;
end;



(*-------- private - class methods --------------------------*)

// set DeprecatedAllowed
class procedure TFormBCP47.SetDeprecatedAllowed(const allow: Boolean);
begin
  if allow = FDeprecatedAllowed then Exit;
  FDeprecatedAllowed:=allow;
  if FormBCP47 <> nil then
  FormBCP47.DeprecatedAllowed_Changed;
end;

// set Codes At First Place
class procedure TFormBCP47.SetCodesAtFirstPlace(const first: Boolean);
begin
  if first = FCodesAtFirstPlace then Exit;
  FCodesAtFirstPlace:=first;
  if FormBCP47 <> nil then
  FormBCP47.CodesAtFirstPlace_Changed;
end;

// set Languages Often Only
class procedure TFormBCP47.SetLanguagesOftenOnly(const only: Boolean);
begin
  if only = FLanguagesOftenOnly then Exit;
  FLanguagesOftenOnly:=only;
  if FormBCP47 <> nil then
  FormBCP47.InitLanguage_Cob;
end;

// set Regions Often Only
class procedure TFormBCP47.SetRegionsOftenOnly(const only: Boolean);
begin
  if only = FRegionsOftenOnly then Exit;
  FRegionsOftenOnly:=only;
  if FormBCP47 <> nil then
  FormBCP47.InitRegion_Cob;
end;

// set Use_ISO639_3_and_5
class procedure TFormBCP47.SetUse_ISO639_3_and_5(const use_3_5: Boolean);
begin
  if use_3_5 = FUse_ISO639_3_and_5 then Exit;
  FUse_ISO639_3_and_5:=use_3_5;
  if FormBCP47 <> nil then
  FormBCP47.InitLanguage_Cob;
end;

// set normalization auto
class procedure TFormBCP47.SetNormalizationAuto(const nf: TNormalizationForm);
begin
  if nf = FNormalizationAuto then Exit;
  FNormalizationAuto:=nf;
  if FormBCP47 <> nil then
  FormBCP47.InitNormalizationAuto;
end;



(*-------- public ----------*)


// set often used languages
class procedure TFormBCP47.Set_OftenUsedLangs(const sl: TStringList);
var
  i: Integer;
  pLang: PLanguage;
begin
  if FLanguagesOftenUsed = nil then
  FLanguagesOftenUsed:=TStringList.Create
  else
  FLanguagesOftenUsed.Clear;

  if sl = nil then Exit;
  for i:=0 to sl.Count -1 do
  begin
    pLang:=Find_Language(sl[i]);
    if pLang = nil then Continue;
    FLanguagesOftenUsed.AddObject(sl[i], TObject(pLang));
  end;

  if FormBCP47 <> nil then
  FormBCP47.InitLanguage_Cob;
end;

// set often used Regions
class procedure TFormBCP47.Set_OftenUsedRegions(const sl: TStringList);
var
  i: Integer;
  aRegion: PRegion;
begin
  if FRegionsOftenUsed = nil then
  FRegionsOftenUsed:=TStringList.Create
  else
  FRegionsOftenUsed.Clear;

  if sl = nil then Exit;
  for i:=0 to sl.Count -1 do
  begin
    aRegion:=Find_Region(sl[i]);
    if aRegion = nil then Continue;
    FRegionsOftenUsed.AddObject(sl[i], TObject(aRegion));
  end;

  if FormBCP47 <> nil then
  FormBCP47.InitRegion_Cob;
end;




// Translate - set language
procedure TFormBCP47.Translate(const ALang: String);
begin
  TranslateUnitResourceStrings('BCP_47_RS', 'languages/bcp_47_rs.%s.po',
    ALang, ALang);
  FNewUILang:=true;

  // Form translate
  Lbl_Edit.Caption:=rsLbl_EditLanguage;
  Lbl_FreeInput.Caption:=rsLbl_FreeInput;
  DiB_Individual.Caption:=rsDiB_Individual;
  Lbl_Language.Caption:=rsLbl_Language;
  Lbl_ExtLang.Caption:=rsLbl_ExtLang;
  Lbl_Script.Caption:=rsLbl_Script;
  Lbl_Region.Caption:=rsLbl_Region;
  Lbl_Variants.Caption:=rsLbl_Variants;
  Lbl_Extensions.Caption:=rsLbl_Extensions;
  Lbl_PrivateUse.Caption:=rsLbl_PrivateUse;
  Lbl_Grandfathered.Caption:=rsLbl_Grandfathered;
  Lbl_StatusSection.Caption:=rsLbl_StatusSection;
  Lbl_Status.Caption:=rsLbl_Status;
  SpB_OK.Caption:=rsSpB_OK;
  SpB_Cancel.Caption:=rsSpB_Cancel;
  SpB_Normalization.Caption:=rsSpB_Normalization;
  Mit_CanonicalOnce.Caption:=rsMit_CanonicalOnce;
  Mit_ExtLangOnce.Caption:=rsMit_ExtLangOnce;
  Mit_CanonicalAuto.Caption:=rsMit_CanonicalAuto;
  Mit_ExtLangAuto.Caption:=rsMit_ExtLangAuto;
  Mit_NoNormalization.Caption:=rsMit_NoNormalization;

  Translate_CoBItems;
  Change_CompsWidth;
end;

// edit a language tag
procedure TFormBCP47.EditLanguage(const aLang: String);
begin
  CoB_FreeInput.Text:=aLang;
  ShowModal;
end;



// TODO:






{--------- TNewComps ----------------------------------------------------------}

// destructor
destructor TNewComps.Destroy;
begin
  aButton.Free;
  aInput.Free;
  inherited Destroy;
end;



{-------- TNewComps2 ----------------------------------------------------------}

// destructor
destructor TNewComps2.Destroy;
begin
  aInput2.Free;
  inherited Destroy;
end;



{-------- TStatusComps --------------------------------------------------------}


// destructor
destructor TStatusComps.Destroy;
begin
  if Lbl_StatusType <> nil then
  Lbl_StatusType.Free;
  Lbl_StatusText.Free;
  Img_StatusType.Free;
  inherited Destroy;
end;














end.

