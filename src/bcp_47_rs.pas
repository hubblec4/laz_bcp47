unit BCP_47_RS;

{$mode objfpc}{$H+}

interface

resourcestring
  rsNoValidBCPStructure = 'The value does not adhere to the general structure of IETF BCP 47/RFC 5646 language tags.';
  rsNoValidLanguageCode = 'The value ''%s'' is not a valid ISO 639 language code.';
  rsNoSupportLanguage4Letter = 'Four-letter language codes are reserved for future use and not supported.';
  rsNoSupportLanguage5_8Letter = 'Five- to eight-letter language codes are currently not supported.';
  rsNoValidExtLangCode = 'The value ''%s'' is not part of the IANA Language Subtag Registry for extended language subtags.';
  rsNoValidExtLangPrefix = 'The extended language ''%0:s'' can be used only with the prefix ''%1:s''.';
  rsNoValidScriptCode = 'The value ''%s'' is not a valid ISO 15924 Script code.';
  rsNoValidRegionNumber = 'The value ''%s'' is not a valid UN M.49 country number code.';
  rsNoValidRegionCode = 'The value ''%s'' is not a valid ISO 3166-1 country code.';
  rsNoValidVariantCode = 'The value ''%s'' is not part of the IANA Language Subtag Registry for language variants.';
  rsNoValidExtensionsId = 'The value ''%s'' is not a registered IANA Extensions tag identifier.';
  rsExtensionsIdDuplicated = 'The Extensions ''%0:s'' and ''%1:s'' start with the same identifier ''%2:s'', which is invalid.';

  rsValidLanguageTag = 'The language tag is valid.';
  rsNoValidLanguageTag = 'The language tag is not valid.';
  rsWarningLanguageTag = 'The language ''%s'' is not an ISO 639-2 code. Players that only support the legacy Matroska language elements but not the IETF BCP 47 language elements will therefore display a different language such as ''und'' (undetermined).';
  rsErrorVariantDouble = 'The variant ''%s'' occurs more than once.';

  (* Warnings *)
  rsWarnSuppressScript  = 'The script ''%0:s'' should not be used for the language ''%1:s'' as it is the script the overwhelming majority of documents for this language is written in.';
  rsWarnDeprecatedLang  = 'The language ''%s'' is deprecated.';
  rsWarnDeprecatedRegion = 'The region ''%s'' is deprecated.';
  rsWarnDeprecatedVariant = 'The variant ''%s'' is deprecated.';
  rsWarnDeprecatedGrandfathered = 'The grandfathered ''%s'' is deprecated.';
  rsWarnVariantPrefix = 'The variant ''%0:s'' is used with prefixes that aren''t suited for it. Suitable prefixes are: %1:s.';
  rsWarnCanonical = 'The corresponding canonical form is: %s.';

  (* Info *)
  rsInfoExtLangForm = 'The corresponding extended language subtags form is: %s.';

  rsUndetermined = 'Undetermined';

  (* Form *)
  rsLbl_EditLanguage = 'Edit language';
  rsLbl_FreeInput    = 'Free-form input';
  rsDiB_Individual   = 'Individually selected components';
  rsLbl_Language     = 'Language:';
  rsLbl_ExtLang      = 'Extended language:';
  rsLbl_Script       = 'Script:';
  rsLbl_Region       = 'Region:';
  rsLbl_Variants     = 'Variants:';
  rsLbl_Extensions   = 'Extensions:';
  rsLbl_PrivateUse   = 'Private use:';
  rsLbl_Grandfathered= 'Grandfathered:';
  rsLbl_StatusSection= 'Status';
  rsLbl_Status       = 'Status:';
  rsLbl_ParserError  = 'Parser error:';
  rsLbl_Infomation   = 'Information:';
  rsLbl_Warnings     = 'Warning(s):';
  rsSpB_OK           = 'OK';
  rsSpB_Cancel       = 'Cancel';
  rsSpB_Normalization= 'Replace with normalized form';
  rsMit_CanonicalOnce= 'Replace with canonical form once';
  rsMit_ExtLangOnce  = 'Replace with extended language subtags form once';
  rsMit_CanonicalAuto= 'Always automatically replace with canonical form';
  rsMit_ExtLangAuto  = 'Always automatically replace with extended language subtags form';
  rsMit_NoNormalization = 'No normalization';



implementation

end.

