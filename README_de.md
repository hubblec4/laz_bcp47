# Laz_BCP47
Laz_BCP47 ist ein Package für Lazarus womit man BCP 47 Sprach-Tags erstellen und bearbeiten kann.  
Im Package ist ein Formular (FormBCP47) enthaten um das Bearbeiten/Erstellen der Subtags zu ermöglichen.  
i18n wird verwendet um das Projekt in verschiedene Sprachen übersetzen zu können.  
![FormBCP47](./docu/pics/FormBCP47_de.jpg)

Weiterhin gibt es im Projekt noch ein Beispielprogramm, was die Verwendung des Laz_BCP47 Packages demonstriert.  
![FormBCP47 Beispiel](./docu/pics/FormBCP47_Example_de.jpg)

## Was ist BCP 47
Dies sind die wichtigsten Seiten wo man nachlesen kann was BCP 47 ist.  
[BCP 47 RFC Editor](https://www.rfc-editor.org/info/bcp47)  
[Wikipedia](https://en.wikipedia.org/wiki/IETF_language_tag)  
[W3C](https://www.w3.org/International/articles/language-tags/)  
[IETF BCP 47](https://tools.ietf.org/search/bcp47)  

Die [IANA Subtag-Registrierung](https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry) enthält alle zugelassenen Codes aller Subtags für die BCP 47.

## Eigenschaften von Laz_BCP47
Für das Projekt war es notwendig weitere Information zusammen zu tragen, da die IANA Subtag-Registrierung nur die kürzesten Codes enthält.  
Zum Beispiel werden für die Sprach-Subtags die ISOs [639-2](https://www.loc.gov/standards/iso639-2/php/code_list.php), [639-3](https://iso639-3.sil.org/sites/iso639-3/files/downloads/iso-639-3.tab) und [639-5](https://de.wikipedia.org/wiki/Liste_der_ISO-639-5-Codes) verwendet.  
Laz_BCP47 unterstützt die Alpha-2 und Alpha-3 sowie die Terminologischen Sprach-Codes.

Für die Schrift-Subtags wird die [ISO 15924](https://unicode.org/iso15924/iso15924.txt.zip) verwendet.  
Bei den Region-Subtags wird die [ISO 3166](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) und die [UN M.49](https://unstats.un.org/unsd/methodology/m49/overview) verwendet.

### Sprachenauswahl
In der IANA Subtag-Registrierung gibt es über 8000 Spracheinträge, was zu einer sehr langen Auswahlliste führt.  
Deshalb habe ich eine Möglickeit eingebaut mit der man nur die ISO 639-2 Sprachen in die Auswahlliste übernehmen kann.

### Sprachen und Regionen Favoriten
Für die Sprach-Subtags und die Region-Subtags gibt es die Möglichkeit Favoriten zu definieren die in den Auswahllisten dann an oberster Stelle stehen.  
Weiterhin gibt es eine Option, um nur die Favoriten in den Auswahllisten zu verwenden.

### veraltete Codes
Da in den meisten ISOs viele veraltete Codes enthalten sind, gibt es die Möglichkeit diese auszublenden, wodurch dann die Auswahllisten verkleinert werden.

### Code Position
Für die Subtag-Beschriftung wird der Code und der Name des Subtags verwendet und es gibt die Möglichkeit den Code vor den Namen zu setzen oder andersrum.  
Zum Beispiel: "de,ger German" oder "German (de,ger)"

### Freiform-Eingabe
Für die Freiform-Eingabe verwende ich eine Combobox, um darin alle gültigen Sprach-Tags zu speichern die verwendet wurden.  
Ein Programm was Laz_BCP47 nutzt, kann diese Eingaben speichern und bei Bedarf wiederherstellen.