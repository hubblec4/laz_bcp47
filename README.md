# Laz_BCP47
Laz_BCP47 is a package for Lazarus to create and edit BCP 47 language tags.  
The package includes a Form (FormBCP47) to enable editing/creating the subtags.  
i18n is used to translate the project into different languages.  
![FormBCP47](./docu/pics/FormBCP47.jpg)

Furthermore, there is an example program in the project, which demonstrates the use of the Laz_BCP47 package.  
![FormBCP47 Beispiel](./docu/pics/FormBCP47_Example.jpg)

## What is BCP 47
These are the most important pages where you can read what BCP 47 is.  
[BCP 47 RFC Editor](https://www.rfc-editor.org/info/bcp47)  
[Wikipedia](https://en.wikipedia.org/wiki/IETF_language_tag)  
[W3C](https://www.w3.org/International/articles/language-tags/)  
[IETF BCP 47](https://tools.ietf.org/search/bcp47)  

The [IANA Subtag-Registry](https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry) contains all approved codes of all subtags for the BCP 47.

## Properties of Laz_BCP47
For the project it was necessary to carry additional information together, as the IANA Subtag-Registry contains only the shortest codes.  
For example, the ISOs [639-2](https://www.loc.gov/standards/iso639-2/php/code_list.php), [639-3](https://iso639-3.sil.org/sites/iso639-3/files/downloads/iso-639-3.tab) and [639-5](https://de.wikipedia.org/wiki/Liste_der_ISO-639-5-Codes) are used for the language subtags.  
Laz_BCP47 supports Alpha-2 and Alpha-3 as well as the terminology language codes.

For the Script subtags, the [ISO 15924](https://unicode.org/iso15924/iso15924.txt.zip) is used.  
The Region-subtags uses the [ISO 3166](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) and [UN M.49](https://unstats.un.org/unsd/methodology/m49/overview).

### Languages selection
In the IANA Subtag-Registry, there are over 8000 language entries, resulting in a very long selection list.  
That's why I installed a possibility with which you can only take the ISO 639-2 languages in the selection list.

### Languages and Regions Favorites
For the language subtags and the region subtags, there is the possibility to define favorites which are in the selection lists then at the top.  
Furthermore, there is an option to use only the favorites in the selection lists.

### Deprecated codes
Since many deprecated codes are included in most ISOs, there is the possibility to hide it, which then reduces the selection lists.

### Code position
For the subtag caption, the code and the name of the subtag is used and there is the possibility to set the code in front of the name or the other way around.  
For example: "en,eng English" or "English (en,eng)"

### Free-form input
For Free-form input, I use a combo box to save all valid Language-Tags that were used.  
A program which uses Laz_BCP47 can store these inputs and restore if necessary.

---
Laz_BCP47 in [German](README_de.md)